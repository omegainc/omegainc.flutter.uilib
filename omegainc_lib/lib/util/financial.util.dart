import 'package:omegainc_lib/enuns/bandeira.cartao.enum.dart';

class FinancialUtil {
  Map<BandeiraCartao, Set<List<String>>> cardNumPatterns =
      <BandeiraCartao, Set<List<String>>>{
    BandeiraCartao.hipercard: <List<String>>{
      <String>['38'],
      <String>['60'],
    },
    BandeiraCartao.jcb: <List<String>>{
      <String>['35'],
    },
    BandeiraCartao.aura: <List<String>>{
      <String>['50'],
    },
    BandeiraCartao.visa: <List<String>>{
      <String>['4'],
    },
    BandeiraCartao.americanExpress: <List<String>>{
      <String>['34'],
      <String>['37'],
    },
    BandeiraCartao.discover: <List<String>>{
      <String>['6011'],
      <String>['622126', '622925'],
      <String>['644', '649'],
      <String>['65']
    },
    BandeiraCartao.mastercard: <List<String>>{
      <String>['51', '55'],
      <String>['2221', '2229'],
      <String>['223', '229'],
      <String>['23', '26'],
      <String>['270', '271'],
      <String>['2720'],
    },
    BandeiraCartao.elo: <List<String>>{
      <String>['636368'],
      <String>['438935'],
      <String>['504175'],
      <String>['451416'],
      <String>['509048'],
      <String>['509067'],
      <String>['509049'],
      <String>['509069'],
      <String>['509050'],
      <String>['509074'],
      <String>['509068'],
      <String>['509040'],
      <String>['509045'],
      <String>['509051'],
      <String>['509046'],
      <String>['509047'],
      <String>['509042'],
      <String>['509052'],
      <String>['509043'],
      <String>['509064'],
      <String>['509040'],
      <String>['36297'],
      <String>['5067'],
      <String>['4576'],
      <String>['4011'],
    },
  };

  BandeiraCartao detectCCType(String cardNumber) {
    BandeiraCartao cardType = BandeiraCartao.otherBrand;

    if (cardNumber.isEmpty) {
      return cardType;
    }

    cardNumPatterns.forEach(
      (BandeiraCartao type, Set<List<String>> patterns) {
        for (List<String> patternRange in patterns) {
          String ccPatternStr =
              cardNumber.replaceAll(RegExp(r'\s+\b|\b\s'), '');
          final int rangeLen = patternRange[0].length;

          if (rangeLen < cardNumber.length) {
            ccPatternStr = ccPatternStr.substring(0, rangeLen);
          }

          if (patternRange.length > 1) {
            final int ccPrefixAsInt = int.parse(ccPatternStr);
            final int startPatternPrefixAsInt = int.parse(patternRange[0]);
            final int endPatternPrefixAsInt = int.parse(patternRange[1]);

            if (ccPrefixAsInt >= startPatternPrefixAsInt &&
                ccPrefixAsInt <= endPatternPrefixAsInt) {
              cardType = type;
              break;
            }
          } else {
            if (ccPatternStr == patternRange[0]) {
              cardType = type;
              break;
            }
          }
        }
      },
    );

    return cardType;
  }

  String getCardBin(String cardNumber) {
    String bin;
    switch (detectCCType(cardNumber)) {
      case BandeiraCartao.visa:
        bin = 'Visa';
        break;

      case BandeiraCartao.americanExpress:
        bin = 'Amex';
        break;

      case BandeiraCartao.mastercard:
        bin = 'Master';
        break;

      case BandeiraCartao.discover:
        bin = 'Discover';
        break;

      case BandeiraCartao.elo:
        bin = 'Elo';
        break;

      default:
        bin = '';
        break;
    }

    return bin;
  }

  String getCardTypeIcon(String cardNumber) {
    String icon;
    switch (detectCCType(cardNumber)) {
      case BandeiraCartao.visa:
        icon = 'assets/img/icons/visa.png';
        break;

      case BandeiraCartao.americanExpress:
        icon = 'assets/img/icons/amex.png';
        break;

      case BandeiraCartao.mastercard:
        icon = 'assets/img/icons/mastercard.png';
        break;

      case BandeiraCartao.discover:
        icon = 'assets/img/icons/discover.png';
        break;

      default:
        icon = '';
        break;
    }

    return icon;
  }
}

import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class MaskUtil {
  MaskTextInputFormatter get maskCep {
    return MaskTextInputFormatter(
      mask: '##.###-###',
      filter: {
        "#": RegExp(r'[0-9]'),
      },
    );
  }

  MaskTextInputFormatter get maskCEP {
    return MaskTextInputFormatter(
      mask: '##.###-###',
      filter: {
        "#": RegExp(r'[0-9]'),
      },
    );
  }

  MaskTextInputFormatter get maskExpiryCard {
    return MaskTextInputFormatter(
      mask: '##/##',
      filter: {
        "#": RegExp(r'[0-9]'),
      },
    );
  }

  MaskTextInputFormatter get maskExpiryCardFull {
    return MaskTextInputFormatter(
      mask: '##/####',
      filter: {
        "#": RegExp(r'[0-9]'),
      },
    );
  }

  MaskTextInputFormatter get maskCVV {
    return MaskTextInputFormatter(
      mask: '###',
      filter: {
        "#": RegExp(r'[0-9]'),
      },
    );
  }

  MaskTextInputFormatter get maskCPF {
    return MaskTextInputFormatter(
      mask: '###.###.###-##',
      filter: {
        "#": RegExp(r'[0-9]'),
      },
    );
  }

  MaskTextInputFormatter get maskCNPJ {
    return MaskTextInputFormatter(
      mask: '##.###.###/####-##',
      filter: {
        "#": RegExp(r'[0-9]'),
      },
    );
  }

  MaskTextInputFormatter get maskTelefone {
    return MaskTextInputFormatter(
      mask: '(##) ####-####',
      filter: {
        "#": RegExp(r'[0-9]'),
      },
    );
  }

  MaskTextInputFormatter get maskCelular {
    return MaskTextInputFormatter(
      mask: '(##) # ####-####',
      filter: {
        "#": RegExp(r'[0-9]'),
      },
    );
  }

  MaskTextInputFormatter get maskData {
    return MaskTextInputFormatter(
      mask: '##/##/####',
      filter: {
        "#": RegExp(r'[0-9]'),
      },
    );
  }

  MaskTextInputFormatter get maskHora {
    return MaskTextInputFormatter(
      mask: '##:##:##',
      filter: {
        "#": RegExp(r'[0-9]'),
      },
    );
  }
}

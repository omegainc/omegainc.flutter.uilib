// ignore: import_of_legacy_library_into_null_safe
class ValidateUtil {
  static String makeValidateFieldMessage(String fieldName) {
    return 'O Campo "$fieldName" é obrigatório';
  }

  static String makeValidateInvalidFieldMessage(String fieldName) {
    return '"$fieldName" inválido';
  }

  static String? validateStringField(String? value, String fieldName) {
    if (value == null || value.isEmpty || value.length < 1) {
      return makeValidateFieldMessage(fieldName);
    }

    return null;
  }

  static String? validateIsNull(dynamic value, String fieldName) {
    if (value == null) {
      return makeValidateFieldMessage(fieldName);
    }

    return null;
  }

  static String? validateNumber(double? value, String fieldName) {
    if (value == null || value <= 0) {
      return makeValidateInvalidFieldMessage(fieldName);
    }

    return null;
  }

  static String? validateNumberInt(int? value, String fieldName) {
    if (value == null || value <= 0) {
      return makeValidateFieldMessage(fieldName);
    }

    return null;
  }

  static String? validateDateStringPtBr(String? value, String fieldName) {
    if (value == null || value.isEmpty || value.length < 1) {
      return makeValidateFieldMessage(fieldName);
    }

    var dateArray = value.split('/');

    var day = dateArray[0];
    var mes = dateArray[1];

    if (int.parse(day) < 1 || int.parse(day) > 31) {
      return makeValidateInvalidFieldMessage(fieldName + ' - Dia');
    }

    if (int.parse(mes) < 1 || int.parse(mes) > 12) {
      return makeValidateInvalidFieldMessage(fieldName + ' - Mês');
    }

    return null;
  }

  String? validateCPF(String value, String fieldName) {
    var fieldCheck = validateStringField(value, fieldName);

    if (fieldCheck != null) {
      return fieldCheck;
    }

    // if (!CPF.isValid(value)) {
    //   return makeValidateInvalidFieldMessage(fieldName);
    // } else {
    //   return null;
    // }
  }
}

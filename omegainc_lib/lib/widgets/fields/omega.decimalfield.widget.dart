import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:omegainc_lib/formatters/decimal.textinputformatter.dart';

import '../../omegalibconfig.dart';

// ignore: must_be_immutable
class OmegaDecimalField extends StatefulWidget {
  bool visible;
  bool enabled;
  Color? labelColor = Colors.black;
  Color? iconColor;
  bool showRequiredLabel = false;
  bool autofocus = false;
  String? label = "";
  double labelSize = 14;
  String? hint = "";

  TextStyle? labelStyle;

  String? Function(double?)? validator;
  Function(double)? onChanged;
  Function(double?)? onSaved;
  Function()? onTap;
  Function(dynamic)? onSelect;
  FocusNode? focusNode = new FocusNode();

  double? initialValue;

  OmegaDecimalField({
    Key? key,
    this.enabled = true,
    this.visible = true,
    this.label,
    this.labelSize = 14,
    this.labelColor = Colors.black,
    this.iconColor,
    this.hint,
    this.onChanged,
    this.onSaved,
    this.onTap,
    this.focusNode,
    this.validator,
    this.showRequiredLabel = false,
    this.autofocus = false,
    this.initialValue,
    this.labelStyle,
  }) : super(key: key);

  @override
  OmegaDecimalFieldState createState() => OmegaDecimalFieldState();
}

class OmegaDecimalFieldState extends State<OmegaDecimalField> {
  final TextEditingController _controller = new TextEditingController();
  double? selectedValue = 0;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) => _initialize());
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _initialize() async {
    if (widget.initialValue != null) {
      this.setSelected(widget.initialValue!);
    }
  }

  /**
   * Set the visibility of the field
   */
  void setVisibility(bool visible) {
    setState(() {
      widget.visible = visible;
    });
  }

  void setSelected(double selected) {
    setState(() {
      selectedValue = selected;

      _controller.text = selected.toString();
    });
  }

  void clearSelection() {
    setState(() {
      selectedValue = null;
      _controller.text = '';
    });
  }

  double _convertValue(String value) {
    value = value.isEmpty ? "0" : value;
    value = value.replaceAll(',', '.');

    double _doubleValue = double.parse(value);

    return _doubleValue;
  }

  @override
  Widget build(BuildContext context) {
    return widget.visible
        ? Container(
            padding: EdgeInsets.only(top: 4, bottom: 4),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 7, bottom: 5, top: 5),
                      child: Text(
                        widget.showRequiredLabel
                            ? widget.label! + ' *'
                            : widget.label!,
                        textAlign: TextAlign.left,
                        style: widget.labelStyle != null
                            ? widget.labelStyle
                            : TextStyle(
                                color: widget.labelColor,
                                fontSize: widget.labelSize,
                              ),
                      ),
                    )
                  ],
                ),
                TextFormField(
                  style: TextStyle(
                    color: widget.enabled ? Colors.black : Colors.grey,
                    fontSize: widget.labelSize,
                  ),
                  enabled: widget.enabled,
                  textCapitalization: TextCapitalization.none,
                  autofocus: widget.autofocus,
                  controller: _controller,
                  focusNode: widget.focusNode,
                  onChanged: (value) {
                    if (widget.onChanged != null) {
                      widget.onChanged!(double.tryParse(value) ?? 0.0);
                    }
                  },
                  onSaved: (value) {
                    if (widget.onSaved != null) {
                      widget.onSaved!(_convertValue(value ?? '0'));
                    }
                  },
                  onTap: widget.onTap,
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintText: widget.hint,
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    contentPadding: EdgeInsets.all(15.0),
                    border: InputBorder.none,
                    enabledBorder: OmegaLibConfig.enabledBorder,
                    focusedBorder: OmegaLibConfig.focusedBorder,
                    focusedErrorBorder: OmegaLibConfig.focusedErrorBorder,
                    errorBorder: OmegaLibConfig.errorBorder,
                    errorStyle: OmegaLibConfig.errorStyle,
                  ),
                  validator: (value) {
                    if (widget.validator == null) {
                      return null;
                    } else {
                      return widget.validator!(_convertValue(value ?? '0'));
                    }
                  },
                )
              ],
            ),
          )
        : Container();
  }
}

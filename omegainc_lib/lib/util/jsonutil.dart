class JSonUtil {
  static double parseDouble(dynamic jsonProperty) {
    double value = 0.0;

    try {
      if (jsonProperty != null) {
        value = double.parse(jsonProperty.toString());
      }
    } catch (e) {
      print(e);
    }

    return value;
  }
}

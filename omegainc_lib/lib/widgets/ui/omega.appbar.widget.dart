import 'package:flutter/material.dart';

class OmegaAppBar extends StatefulWidget implements PreferredSizeWidget {
  @override
  _OmegaAppBarState createState() => _OmegaAppBarState();

  String title;
  String subtitle;
  List<Widget>? actions;
  Widget? leading;
  Widget? logo;
  double titleSize;
  Brightness brightness;
  bool loading = false;

  Color iconDataColor, textColor, bgColor;

  OmegaAppBar({
    this.title = 'Omega',
    this.subtitle = '',
    this.titleSize = 16,
    this.actions,
    this.leading,
    this.logo,
    required this.iconDataColor,
    required this.textColor,
    required this.bgColor,
    this.brightness = Brightness.light,
  });

  @override
  Size get preferredSize => Size(double.infinity, (kToolbarHeight));
}

class _OmegaAppBarState extends State<OmegaAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      iconTheme: IconThemeData(color: widget.iconDataColor),
      centerTitle: false,
      actions: widget.actions,
      leading: widget.leading,
      title: Row(
        children: <Widget>[
          widget.logo != null ? widget.logo! : SizedBox(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.title,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                style: TextStyle(
                  color: widget.textColor,
                  fontSize: widget.titleSize,
                ),
              ),
              (widget.subtitle.isNotEmpty)
                  ? Text(
                      widget.subtitle,
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                      style: TextStyle(
                        color: widget.textColor,
                        fontSize: widget.titleSize - 4,
                      ),
                    )
                  : SizedBox(width: 0, height: 0)
            ],
          )
        ],
      ),
      flexibleSpace: Container(
        decoration: BoxDecoration(color: widget.bgColor),
      ),
      // bottom: OmegaLinearProgressIndicatorWidget(
      //   backgroundColor: Colors.green,
      //   value: 0,
      // ),
    );
  }
}

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:loading_overlay/loading_overlay.dart';

import '../../omegainc_lib.dart';
import 'omega.searchbar.widget.dart';
import '../ui/omega.loading.widget.dart';

class SearchFieldSource {
  dynamic value;
  String label, subtitle;

  SearchFieldSource({this.value, required this.label, this.subtitle = ''});
}

/**
 * Serch field de dados locais
 */
// ignore: must_be_immutable
class OmegaSearchField extends StatefulWidget {
  /**
   * Em fundos brancos utilizar black, em fundos escuros utilizar white
   */
  Color colorWhite;
  Color colorIcon, colorPrimary;
  String label = "";
  Color labelColor = Colors.black;
  double labelSize = 14;

  bool visible;
  bool enabled;

  bool showRequiredLabel = false;
  String? hint = "";
  TextInputType keyboardType = TextInputType.text;

  String? Function(String?)? validator;
  Function(dynamic value, SearchFieldSource? key)? onSaved;
  Function(dynamic)? onSelect;
  Future<List<SearchFieldSource>> Function() getSource;
  bool Function()? validateToOpen;

  bool useValidateToOpen = false;

  List<TextInputFormatter>? inputFormatters = <TextInputFormatter>[];

  bool showPersistenceButton = false;
  Widget? persistencePage;
  String barText;
  dynamic selectedValue;
  String? selectedText;

  OmegaSearchField({
    Key? key,
    this.enabled = true,
    this.visible = true,
    this.label = '',
    this.labelColor = Colors.black,
    required this.colorWhite,
    required this.colorIcon,
    required this.colorPrimary,
    this.hint,
    this.keyboardType = TextInputType.text,
    this.inputFormatters,
    this.barText = 'Buscar',
    this.onSelect,
    this.onSaved,
    this.validator,
    this.useValidateToOpen = false,
    this.validateToOpen,
    this.showRequiredLabel = false,
    this.showPersistenceButton = false,
    this.persistencePage,
    required this.getSource,
    this.labelSize = 14,
  }) : super(key: key);

  @override
  OmegaSearchFieldState createState() => OmegaSearchFieldState();
}

class OmegaSearchFieldState extends State<OmegaSearchField> {
  BuildContext? buildContext;
  SearchFieldSource? selectedResult;
  TextEditingController _controller = new TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  final keyFormNew = new GlobalKey<FormState>();

  void setSelectedById(dynamic id) async {
    List<SearchFieldSource> result = await widget.getSource();

    if (result != null) {
      var selection = result.where((w) => w.value == id)?.first ?? null;

      if (selection != null) {
        resolveSelectedValue(selection);
      }
    }
  }

  void openSearchPage(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SerchFieldPage(
          barText: widget.barText,
          persistencePage: widget.persistencePage,
          showPersistenceButton: widget.showPersistenceButton,
          getSource: widget.getSource,
          colorWhite: widget.colorWhite,
          colorPrimary: widget.colorPrimary,
        ),
        fullscreenDialog: true,
      ),
    );

    if (result != null) resolveSelectedValue(result);
  }

  void resolveSelectedValue(SearchFieldSource result) {
    if (result == null) return;

    if (!mounted) return;

    String selected;
    selectedResult = result;

    setState(() {
      widget.selectedValue = result.value;
    });

    var resolvedWrapedLabel = result.label.replaceAll('\n', ' - ');

    selected = (result.subtitle.isEmpty)
        ? (resolvedWrapedLabel)
        : (resolvedWrapedLabel + ' - ' + result.subtitle);

    _controller.text = selected;
    widget.selectedText = selected;

    if (widget.onSelect != null) {
      widget.onSelect!(widget.selectedValue);
    }
  }

  void clearSelection() {
    if (!mounted) return;

    setState(() {
      widget.selectedValue = null;
    });
    _controller.text = '';
    widget.selectedText = '';
  }

  /**
   * Set the visibility of the field
   */
  void setVisibility(bool visible) {
    setState(() {
      widget.visible = visible;
    });
  }

  void createNew(String label) async {
    return showDialog<void>(
      context: this.context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return Dialog(
          insetPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          backgroundColor: widget.colorPrimary,
          child: Container(
            margin: EdgeInsets.only(top: 10),
            width: double.infinity,
            height: 220,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text(
                    'Novo ' + label,
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(height: 10),
                  Divider(),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: Form(
                      key: keyFormNew,
                      child: Column(
                        children: [
                          OmegaTextField(
                            autofocus: true,
                            hint: 'Informe o $label',
                            keyboardType: TextInputType.text,
                            label: label,
                            labelColor: widget.colorWhite,
                            showRequiredLabel: true,
                            validator: (value) {
                              if (value == null || value.length < 1) {
                                return 'O Campo $label é requerido';
                              } else {
                                return null;
                              }
                            },
                            onSaved: (value) {
                              resolveSelectedValue(new SearchFieldSource(
                                label: value ?? '',
                                value: 0,
                              ));
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      margin: EdgeInsets.only(right: 8, left: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          OmegaButton(
                            height: 40,
                            width: MediaQuery.of(context).size.width * 0.40,
                            color: Colors.red,
                            text: 'Cancelar',
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                          OmegaButton(
                            height: 40,
                            width: MediaQuery.of(context).size.width * 0.45,
                            color: Colors.green,
                            text: 'Confirmar',
                            onPressed: () {
                              FocusManager.instance.primaryFocus?.unfocus();

                              if (keyFormNew.currentState?.validate() ??
                                  false) {
                                keyFormNew.currentState?.save();
                                Navigator.pop(context);
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.visible
        ? Container(
            padding: EdgeInsets.only(top: 4, bottom: 4),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 7, bottom: 5, top: 5),
                      child: Text(
                        widget.showRequiredLabel
                            ? widget.label + ' *'
                            : widget.label,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: widget.labelColor,
                          fontSize: widget.labelSize,
                        ),
                      ),
                    )
                  ],
                ),
                TextFormField(
                  style: TextStyle(
                    color: widget.enabled ? Colors.black : Colors.grey,
                    fontSize: widget.labelSize,
                  ),
                  enabled: widget.enabled,
                  readOnly: true,
                  validator: widget.validator,
                  onSaved: (value) {
                    if (widget.onSaved != null) {
                      widget.onSaved!(value as dynamic, selectedResult);
                    }
                  },
                  onTap: () {
                    if (widget.useValidateToOpen &&
                        widget.validateToOpen != null) {
                      if (widget.validateToOpen!()) {
                        openSearchPage(context);
                      }
                    } else {
                      openSearchPage(context);
                    }
                  },
                  controller: _controller,
                  inputFormatters: widget.inputFormatters,
                  keyboardType: widget.keyboardType,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey[200],
                    suffixIcon: Icon(Icons.search, color: widget.colorIcon),
                    hintText: widget.hint,
                    hintStyle: TextStyle(color: Colors.black),
                    contentPadding: EdgeInsets.all(15.0),
                    border: InputBorder.none,
                    enabledBorder: OmegaLibConfig.enabledBorder,
                    focusedBorder: OmegaLibConfig.focusedBorder,
                    errorBorder: OmegaLibConfig.errorBorder,
                    focusedErrorBorder: OmegaLibConfig.focusedErrorBorder,
                    errorStyle: OmegaLibConfig.errorStyle,
                  ),
                )
              ],
            ),
          )
        : Container();
  }
}

class SerchFieldPage extends StatefulWidget {
  String barText;
  Color colorWhite, colorPrimary;

  bool showPersistenceButton = false;
  Widget? persistencePage;

  Future<List<SearchFieldSource>> Function() getSource;

  SerchFieldPage({
    this.barText = 'Buscar',
    this.showPersistenceButton = false,
    this.persistencePage,
    required this.getSource,
    this.colorWhite = Colors.white,
    this.colorPrimary = Colors.black,
  });

  @override
  _SerchFieldPageState createState() => _SerchFieldPageState();
}

class _SerchFieldPageState extends State<SerchFieldPage> {
  BuildContext? _context;
  bool onSearch = false;
  bool loading = false;
  List<SearchFieldSource> source = [];
  List<SearchFieldSource> searchResults = [];
  FocusNode focusSearch = new FocusNode();

  @override
  void initState() {
    super.initState();
    this.loadSource();
  }

  void filterSearchResults(String query) {
    List<SearchFieldSource> dummySearchList = <SearchFieldSource>[];
    dummySearchList.addAll(searchResults);

    if (query.isNotEmpty) {
      List<SearchFieldSource> dummyListData = <SearchFieldSource>[];

      dummySearchList.forEach((item) {
        if (item.label.toLowerCase().contains(query.toLowerCase())) {
          dummyListData.add(item);
        }
      });

      setState(() {
        source.clear();
        source.addAll(dummyListData);
      });

      return;
    } else {
      setState(() {
        source.clear();
        source.addAll(searchResults);
      });
    }
  }

  Future<void> loadSource() async {
    try {
      setState(() {
        loading = true;
      });

      List<SearchFieldSource> result = await widget.getSource();

      setState(() {
        source = result;
        loading = false;
        this.searchResults.addAll(this.source);
      });
    } catch (error) {
      setState(() {
        loading = false;
      });
      throw error;
    }
  }

  @override
  Widget build(BuildContext context) {
    this._context = context;

    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          onSearch
              ? SizedBox(width: 0, height: 0)
              : IconButton(
                  icon: Icon(
                    Icons.search,
                    color: widget.colorWhite,
                  ),
                  onPressed: () {
                    setState(() {
                      onSearch = true;
                    });
                    focusSearch.requestFocus();
                  },
                ),
          widget.showPersistenceButton
              ? IconButton(
                  icon: Icon(
                    Icons.add,
                    color: widget.colorWhite,
                  ),
                  onPressed: () async {
                    if (widget.persistencePage != null) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => widget.persistencePage!),
                      );
                      setState(() {});
                    }
                  },
                )
              : SizedBox()
        ],
        leading: onSearch
            ? IconButton(
                icon: Icon(
                  Icons.close,
                  color: widget.colorWhite,
                ),
                onPressed: () {
                  setState(() {
                    onSearch = false;
                  });
                },
              )
            : IconButton(
                icon: Icon(
                  Icons.close,
                  color: widget.colorWhite,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
        title: onSearch
            ? OmegaSearchBar(
                focusNode: focusSearch,
                textcolor: widget.colorWhite,
                onChanged: (value) {
                  filterSearchResults(value);
                },
              )
            : InkWell(
                onTap: () {
                  setState(() {
                    onSearch = true;
                  });
                  focusSearch.requestFocus();
                },
                child: Text(
                  widget.barText,
                  style: TextStyle(
                    color: widget.colorWhite,
                  ),
                ),
              ),
        iconTheme: IconThemeData(color: widget.colorWhite),
        flexibleSpace: Container(
          decoration: BoxDecoration(color: widget.colorPrimary),
        ),
      ),
      body: LoadingOverlay(
        isLoading: this.loading,
        progressIndicator: OmegaLoadingWidget(
          textColor: widget.colorWhite,
        ),
        child: Column(
          children: <Widget>[
            Flexible(
              child: (source.length > 0)
                  ? ListView.builder(
                      shrinkWrap: true,
                      itemCount: source.length,
                      itemBuilder: (context, index) {
                        var item = source[index];

                        return ListTile(
                          title: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(item.label),
                                  item.subtitle.isNotEmpty
                                      ? Text(item.subtitle)
                                      : SizedBox(height: 0, width: 0)
                                ],
                              )
                            ],
                          ),
                          onTap: () {
                            Navigator.pop(context, item);
                          },
                        );
                      },
                    )
                  : ListTile(
                      title: Text(
                          loading ? 'Carregando...' : 'Nenhum item encontrado'),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}

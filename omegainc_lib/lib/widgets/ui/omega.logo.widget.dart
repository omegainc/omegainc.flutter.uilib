import 'package:flutter/material.dart';

class OmegaLogoWidget extends StatelessWidget {
  String path;

  OmegaLogoWidget(this.path);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.zero,
      padding: EdgeInsets.only(right: 5, left: 0),
      child: Image.asset(
        path,
        width: 42,
        height: 42,
      ),
    );
  }
}

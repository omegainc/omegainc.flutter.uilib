import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:loading_overlay/loading_overlay.dart';

import '../../omegalibconfig.dart';
import 'omega.searchbar.widget.dart';
import '../ui/omega.centerfloating.widget.dart';
import '../ui/omega.loading.widget.dart';
import 'omega.searchfield.widget.dart';

// ignore: must_be_immutable
class OmegaSearchFieldMulti extends StatefulWidget {
  bool visible;
  bool enabled;
  Color colorWhite, colorIcon, colorPrimary;
  Color labelColor = Colors.black;

  String label = "";
  double labelSize = 14;
  bool showRequiredLabel = false;
  String? hint = "";
  bool obscureText = false;
  TextInputType keyboardType = TextInputType.text;

  String? Function(String?)? validator;
  Function(dynamic value, List<SearchFieldSource>? keys)? onSaved;
  Function(dynamic)? onSelect;
  List<SearchFieldSource> Function() getSource;
  bool Function()? validateToOpen;

  FocusNode? focusNode = new FocusNode();
  bool useValidateToOpen = false;

  List<TextInputFormatter>? inputFormatters = <TextInputFormatter>[];

  List<SearchFieldSource> source;
  String barText;
  List<SearchFieldSource>? selectedValue;

  OmegaSearchFieldMulti({
    Key? key,
    this.enabled = true,
    this.visible = true,
    this.label = '',
    this.labelColor = Colors.black,
    required this.colorWhite,
    required this.colorIcon,
    required this.colorPrimary,
    this.hint,
    this.obscureText = false,
    this.keyboardType = TextInputType.text,
    this.focusNode,
    this.inputFormatters,
    required this.source,
    this.barText = 'Buscar',
    this.onSelect,
    this.onSaved,
    this.validator,
    this.showRequiredLabel = false,
    required this.getSource,
    this.useValidateToOpen = false,
    this.validateToOpen,
    this.labelSize = 14,
  }) : super(key: key);

  @override
  OmegaSearchFieldMultiState createState() => OmegaSearchFieldMultiState();
}

class OmegaSearchFieldMultiState extends State<OmegaSearchFieldMulti> {
  List<SearchFieldSource>? selectedResult;
  TextEditingController _controller = new TextEditingController();

  void openSearchPage(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => OmegaSearchFieldMultiPage(
          barText: widget.barText,
          getSource: widget.getSource,
          colorWhite: widget.colorWhite,
          colorPrimary: widget.colorPrimary,
        ),
        fullscreenDialog: true,
      ),
    );

    resolveSelectedValue(result);
  }

  void resolveSelectedValue(List<SearchFieldSource> result) {
    selectedResult = result;
    setState(() {
      widget.selectedValue = result;
    });

    String selectedString = "";

    for (int i = 0; i < result.length; i++) {
      var label = result[i].label;

      if (i + 1 == result.length) {
        selectedString += "$label";
      } else {
        selectedString += "$label, ";
      }
    }

    _controller.text = selectedString;

    if (widget.onSelect != null) widget.onSelect!(widget.selectedValue);
  }

  void clearSelection() {
    setState(() {
      widget.selectedValue = null;
    });
    _controller.text = '';
    widget.selectedValue = [];
  }

  /**
   * Set the visibility of the field
   */
  void setVisibility(bool visible) {
    setState(() {
      widget.visible = visible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.visible
        ? Container(
            padding: EdgeInsets.only(top: 4, bottom: 4),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 7, bottom: 5, top: 5),
                      child: Text(
                        widget.showRequiredLabel
                            ? widget.label + ' *'
                            : widget.label,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: widget.labelColor,
                          fontSize: widget.labelSize,
                        ),
                      ),
                    )
                  ],
                ),
                TextFormField(
                  style: TextStyle(
                    color: widget.enabled ? Colors.black : Colors.grey,
                    fontSize: widget.labelSize,
                  ),
                  enabled: widget.enabled,
                  readOnly: true,
                  validator: widget.validator,
                  onSaved: (value) {
                    if (widget.onSaved != null) {
                      widget.onSaved!(value as dynamic, selectedResult);
                    }
                  },
                  onTap: () {
                    if (widget.useValidateToOpen &&
                        widget.validateToOpen != null) {
                      if (widget.validateToOpen!()) {
                        openSearchPage(context);
                      }
                    } else {
                      openSearchPage(context);
                    }
                  },
                  controller: _controller,
                  inputFormatters: widget.inputFormatters,
                  focusNode: widget.focusNode,
                  keyboardType: widget.keyboardType,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey[200],
                    suffixIcon: Icon(Icons.search, color: widget.colorIcon),
                    hintText: widget.hint,
                    hintStyle: TextStyle(color: Colors.black),
                    contentPadding: EdgeInsets.all(15.0),
                    border: InputBorder.none,
                    enabledBorder: OmegaLibConfig.enabledBorder,
                    focusedBorder: OmegaLibConfig.focusedBorder,
                    focusedErrorBorder: OmegaLibConfig.focusedErrorBorder,
                    errorBorder: OmegaLibConfig.errorBorder,
                    errorStyle: OmegaLibConfig.errorStyle,
                  ),
                  obscureText: widget.obscureText,
                )
              ],
            ),
          )
        : Container();
  }
}

// ignore: must_be_immutable
class OmegaSearchFieldMultiPage extends StatefulWidget {
  String barText;
  List<SearchFieldSource> Function() getSource;
  Color colorWhite, colorPrimary;

  OmegaSearchFieldMultiPage({
    this.barText = 'Buscar',
    required this.getSource,
    this.colorWhite = Colors.white,
    this.colorPrimary = Colors.black,
  });

  @override
  _OmegaSearchFieldMultiPageState createState() =>
      _OmegaSearchFieldMultiPageState();
}

class _OmegaSearchFieldMultiPageState extends State<OmegaSearchFieldMultiPage> {
  FocusNode focusSearch = new FocusNode();
  bool onSearch = false;
  bool loading = false;
  List<SearchFieldSource> source = [];
  List<SearchFieldSource> searchResults = [];
  List<SearchFieldSource> selectedItems = <SearchFieldSource>[];

  @override
  void initState() {
    super.initState();
    this.loadSource();
  }

  void filterSearchResults(String query) {
    List<SearchFieldSource> dummySearchList = <SearchFieldSource>[];
    dummySearchList.addAll(searchResults);

    if (query.isNotEmpty) {
      List<SearchFieldSource> dummyListData = <SearchFieldSource>[];

      dummySearchList.forEach((item) {
        if (item.label.toLowerCase().contains(query.toLowerCase())) {
          dummyListData.add(item);
        }
      });

      setState(() {
        source.clear();
        source.addAll(dummyListData);
      });

      return;
    } else {
      setState(() {
        source.clear();
        source.addAll(searchResults);
      });
    }
  }

  Future<void> loadSource() async {
    try {
      setState(() {
        loading = true;
      });

      List<SearchFieldSource> result = await widget.getSource();

      setState(() {
        source = result;
        loading = false;
        this.searchResults.addAll(this.source);
      });
    } catch (error) {
      setState(() {
        loading = false;
      });
      throw error;
    }
  }

  void close(BuildContext context) {
    Navigator.pop(context, selectedItems);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            onSearch
                ? SizedBox(
                    width: 1,
                  )
                : IconButton(
                    icon: Icon(
                      Icons.search,
                      color: widget.colorWhite,
                    ),
                    onPressed: () {
                      setState(() {
                        onSearch = true;
                      });
                      focusSearch.requestFocus();
                    },
                  )
          ],
          leading: onSearch
              ? IconButton(
                  icon: Icon(
                    Icons.close,
                    color: widget.colorWhite,
                  ),
                  onPressed: () {
                    setState(() {
                      onSearch = false;
                    });
                  },
                )
              : IconButton(
                  icon: Icon(
                    Icons.close,
                    color: widget.colorWhite,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
          title: onSearch
              ? OmegaSearchBar(
                  focusNode: focusSearch,
                  textcolor: widget.colorWhite,
                  onChanged: (value) {
                    filterSearchResults(value);
                  },
                )
              : InkWell(
                  onTap: () {
                    setState(() {
                      onSearch = true;
                    });
                    focusSearch.requestFocus();
                  },
                  child: Text(
                    widget.barText,
                    style: TextStyle(
                      color: widget.colorWhite,
                    ),
                  ),
                ),
          flexibleSpace: Container(
            decoration: BoxDecoration(color: widget.colorPrimary),
          ),
        ),
        floatingActionButton: OmegaCenterFloatingWidget(
          color: Colors.green,
          icon: Icons.check,
          label: 'Salvar',
          onPressed: () {
            close(context);
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: BottomAppBar(
          color: widget.colorWhite,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          ),
        ),
        body: LoadingOverlay(
          isLoading: this.loading,
          progressIndicator: OmegaLoadingWidget(
            textColor: widget.colorWhite,
          ),
          child: Column(
            children: <Widget>[
              Flexible(
                child: (source.length > 0)
                    ? ListView.builder(
                        shrinkWrap: true,
                        itemCount: source.length,
                        itemBuilder: (context, index) {
                          var item = source[index];

                          return CheckboxListTile(
                            title: Text(item.label),
                            onChanged: (bool? value) {
                              setState(() {
                                if (value!) {
                                  selectedItems.add(item);
                                } else {
                                  selectedItems.remove(item);
                                }
                              });
                            },
                            value: selectedItems.contains(item),
                          );
                        },
                      )
                    : ListTile(
                        title: Text(loading
                            ? 'Carregando...'
                            : 'Nenhum item encontrado'),
                      ),
              ),
            ],
          ),
        ));
  }
}

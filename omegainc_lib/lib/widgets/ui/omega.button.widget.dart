import 'dart:ui';

import 'package:flutter/material.dart';

class OmegaButton extends StatelessWidget {
  double? width, height, fontSize, iconSpace, borderRadius, iconSize;
  TextAlign textAlign = TextAlign.center;
  IconData? icon;
  String text;
  Function()? onPressed;
  Color color, iconColor, textColor;
  TextStyle? textStyle;

  OmegaButton({
    required this.text,
    this.icon,
    this.iconSize,
    this.width,
    required this.height,
    this.borderRadius = 10.0,
    this.fontSize = 14,
    this.iconSpace = 0,
    this.onPressed,
    this.color = Colors.green,
    this.iconColor = Colors.grey,
    this.textColor = Colors.white,
    this.textStyle,
    this.textAlign = TextAlign.center,
  });

  bool withIcon = false;

  @override
  Widget build(BuildContext context) {
    withIcon = icon != null;

    return Container(
      padding: EdgeInsets.only(top: 2, bottom: 2),
      margin: EdgeInsets.all(4.0),
      child: SizedBox(
        width: width,
        height: height,
        child: ElevatedButton(
          style: ButtonStyle(
            shape: MaterialStateProperty.resolveWith(
              (states) => RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(borderRadius!),
                side: BorderSide(
                  color: Colors.transparent,
                ),
              ),
            ),
            backgroundColor:
                MaterialStateProperty.resolveWith((states) => color),
          ),
          onPressed: onPressed,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              withIcon
                  ? Container(
                      child: Row(
                        children: <Widget>[
                          Icon(
                            icon,
                            color: iconColor,
                            size: iconSize,
                          ),
                          SizedBox(
                            width: iconSpace,
                          ),
                        ],
                      ),
                    )
                  : Container(),
              Text(
                text,
                textAlign: textAlign,
                style: textStyle != null
                    ? textStyle
                    : TextStyle(
                        color: textColor,
                        fontSize: fontSize,
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_toastr/flutter_toastr.dart';
import 'package:intl/intl.dart';
import 'package:omegainc_lib/omegalibconfig.dart';
import 'package:omegainc_lib/responsive.dart';
import 'package:omegainc_lib/widgets/dialogs/omega.confirm.dialog.widget.dart';
import 'package:omegainc_lib/widgets/dialogs/omega.error.dialog.widget.dart';
import 'package:omegainc_lib/widgets/dialogs/omega.info.dialog.widget.dart';
import 'package:omegainc_lib/widgets/dialogs/omega.loading.dialog.widget.dart';
import 'package:omegainc_lib/widgets/dialogs/omega.success.dialog.widget.dart';

import '../widgets/dialogs/omega.warning.dialog.widget.dart';

class CommonUtil {
  static bool isDebug() {
    bool inDebugMode = false;
    assert(inDebugMode = true);
    return inDebugMode;
  }

  Uint8List? convertImage(String base64Image) {
    try {
      return base64Decode(base64Image);
    } catch (error) {
      return null;
    }
  }

  static double parseDouble(dynamic val) {
    double value = 0.0;

    try {
      if (val != null) {
        value = double.parse(val.toString());
      }
    } catch (e) {
      print(e);
    }

    return value;
  }

  String formatMoney(dynamic value) {
    double dValue = parseDouble(value);

    final formatter = new NumberFormat("#,##0.00", "pt_BR");
    String newText = "R\$ " + formatter.format(dValue);
    return newText;
  }

  String formatDecimal(dynamic value) {
    if (value is int) value = value.toDouble();

    double valor = value == 0 ? 0.0 : value;

    final formatter = new NumberFormat("0.00", "en");
    String newText = formatter.format(valor);
    return newText;
  }

  String formatDecimalPattern(dynamic value, String pattern) {
    if (value is int) value = value.toDouble();

    final formatter = new NumberFormat(pattern, "en");
    String newText = formatter.format(value);
    return newText;
  }

  /*
    * Show a toast message for information
    * @param message
    * @param context
    * @return void
   */
  void toastInfo(String message, BuildContext context) {
    FlutterToastr.show(
      message,
      context,
      duration: 3,
      textStyle: const TextStyle(
        color: Colors.black,
        fontSize: 16.0,
      ),
      backgroundColor: Colors.white,
    );
  }

  /*
    * Show a toast message for success
    * @param message
    * @param context
    * @return void
   */
  void toastSuccess(String message, BuildContext context) {
    FlutterToastr.show(
      message,
      context,
      duration: 3,
      textStyle: const TextStyle(
        color: Colors.white,
        fontSize: 16.0,
      ),
      backgroundColor: Colors.green,
    );
  }

  /*
    * Show a toast message for error
    * @param message
    * @param context
    * @return void
   */
  void toastError(String message, BuildContext context) {
    FlutterToastr.show(
      message,
      context,
      duration: 3,
      textStyle: const TextStyle(
        color: Colors.white,
        fontSize: 16.0,
      ),
      backgroundColor: Colors.red,
    );
  }

  /*
    * Show a toast message for warning
    * @param message
    * @param context
    * @return void
   */
  void toastWarning(String message, BuildContext context) {
    FlutterToastr.show(
      message,
      context,
      duration: 3,
      textStyle: const TextStyle(
        color: Colors.white,
        fontSize: 16.0,
      ),
      backgroundColor: Colors.orange,
    );
  }

  Future<void> showAlert(BuildContext _context, String _title,
      List<Widget> _body, List<Widget> _actions) async {
    return showDialog<void>(
      context: _context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(_title),
          content: SingleChildScrollView(
            child: ListBody(
              children: _body,
            ),
          ),
          actions: _actions,
        );
      },
    );
  }

  Future<void> showCustomDialog(
      BuildContext context, Widget alertDialog) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext _context) {
        return alertDialog;
      },
    );
  }

  Future<void> error(
    BuildContext context,
    String errorMessage,
    Exception error,
  ) async {
    try {
      return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return OmegaErrorDialogWidget(
            description: errorMessage,
            longMessage: error.toString(),
            textColor: OmegaLibConfig.dialogFieldStyle.textColor,
            bgColor: OmegaLibConfig.dialogFieldStyle.bgColor,
          );
        },
      );
    } catch (error) {
      print(error);
    }
  }

  Future<void> errorException(BuildContext context, dynamic error,
      {bool realyError = true}) async {
    String message = 'Erro desconhecido';

    print(error.toString());

    message = error.toString();
    message = message.replaceAll('Exception:', '');

    try {
      return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return OmegaErrorDialogWidget(
            description: message,
            textColor: OmegaLibConfig.dialogFieldStyle.textColor,
            bgColor: OmegaLibConfig.dialogFieldStyle.bgColor,
          );
        },
      );
    } catch (error) {
      print(error);
    }
  }

  Future<void> customError(
      BuildContext context, String title, dynamic message) async {
    String defaultMessage = 'Erro desconhecido';

    try {
      if (message is TypeError) {
        defaultMessage = message.toString();
      } else if (message.message != null) {
        defaultMessage = message.message;
      }
    } catch (error) {
      defaultMessage = message;
    }

    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext ctx) {
        return OmegaErrorDialogWidget(
          title: title,
          description: defaultMessage,
          textColor: OmegaLibConfig.dialogFieldStyle.textColor,
          bgColor: OmegaLibConfig.dialogFieldStyle.bgColor,
        );
      },
    );
  }

  Future<void> success(BuildContext context, String title,
      {String? message}) async {
    if (message == null) {
      message = 'Operação realizada com sucesso!';
    }

    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext ctx) {
        return OmegaSuccessDialogWidget(
          title: title,
          description: message,
          textColor: OmegaLibConfig.dialogFieldStyle.textColor,
          bgColor: OmegaLibConfig.dialogFieldStyle.bgColor,
        );
      },
    );
  }

  Future<void> info(BuildContext context, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext ctx) {
        return OmegaInfoDialogWidget(
          description: message,
          textColor: OmegaLibConfig.dialogFieldStyle.textColor,
          bgColor: OmegaLibConfig.dialogFieldStyle.bgColor,
        );
      },
    );
  }

  Future<void> warning(BuildContext context, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext ctx) {
        return OmegaWarningDialogWidget(
          description: message,
          textColor: OmegaLibConfig.dialogFieldStyle.textColor,
          bgColor: OmegaLibConfig.dialogFieldStyle.bgColor,
        );
      },
    );
  }

  /*
    * Confirm dialog
    * @param context
    * @param title
    * @param message
    * @param onConfirm
    * @param onDeny
    * @return Future<void>
   */
  Future<void> confirm(
    BuildContext context,
    String title,
    String message,
    Function()? onConfirm,
    Function()? onDeny,
  ) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext ctx) {
        return OmegaConfirmDialogWidget(
          title: title,
          description: message,
          noPressed: onDeny,
          yesPressed: onConfirm,
          textColor: OmegaLibConfig.dialogFieldStyle.textColor,
          bgColor: OmegaLibConfig.dialogFieldStyle.bgColor,
        );
      },
    );
  }

  Future<void> loading(
    BuildContext context,
    String title,
    String message,
    Function funcBackground,
  ) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext ctx) {
        return OmegaLoadingDialogWidget(
          funcBackground,
          title: title,
          description: message,
          textColor: OmegaLibConfig.dialogFieldStyle.textColor,
          bgColor: OmegaLibConfig.dialogFieldStyle.bgColor,
        );
      },
    );
  }

  double defineWidthModalSize(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    if (screenWidth >= 2500) {
      return 0.50;
    } else if (Responsive.isDesktop(context)) {
      return 0.50;
    } else if (Responsive.isTablet(context)) {
      return 0.70;
    } else {
      return 0.95;
    }
  }
}

class StringUtil {
  static String limitText(String value, int maxLength) {
    return value.length > maxLength
        ? value.substring(0, maxLength) + "..."
        : value;
  }
}

import 'package:flutter/widgets.dart';

class ResponsiveFontSizeHelper {
  static double sp(BuildContext context, double fontSize) {
    // Get the screen width
    double screenWidth = MediaQuery.of(context).size.width;

    // Choose a reference screen width (e.g., 360) and a reference font size
    const double referenceScreenWidth = 360.0;
    const double referenceFontSize = 14.0;

    // Calculate the ratio of the current screen width to the reference screen width
    double screenWidthRatio = screenWidth / referenceScreenWidth;

    // Calculate the adjusted font size
    double adjustedFontSize = fontSize * screenWidthRatio;

    // Ensure that the font size is not too small or too large
    return adjustedFontSize.clamp(
        referenceFontSize * 0.8, referenceFontSize * 2.0);
  }
}

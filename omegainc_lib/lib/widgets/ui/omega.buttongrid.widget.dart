import 'package:flutter/material.dart';

class OmegaButtonGridWidget extends StatelessWidget {
  Color bgColor;
  Color color;
  Color imgColor;
  String text = "";
  String imageSrc = "";
  void Function()? onClick;

  OmegaButtonGridWidget({
    required this.bgColor,
    required this.color,
    this.imgColor = Colors.white,
    required this.text,
    required this.imageSrc,
    this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: bgColor,
      padding: EdgeInsets.all(8),
      child: Center(
        child: SizedBox(
          height: 110,
          width: 160,
          child: InkWell(
            onTap: onClick,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 52,
                    width: 52,
                    child: Image.asset(
                      imageSrc,
                      color: imgColor,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    text,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 17,
                      color: color,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

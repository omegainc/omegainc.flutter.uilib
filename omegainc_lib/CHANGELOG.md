## [1.0.3] - 2024-11-21

* Dependências atualizadas

## [1.0.2] - 2024-08-20

* Adicionado labelColor para searchfield multi

## [1.0.1] - 2024-05-31.

* Melhoria nos dialogs, incluindo nova configuração para style


## [1.0.0] - .

* Ajustado largura dos dialogs em telas ultra wide
* Implementado configuração padrão para as cores dos dialogs

## [0.1.7] - 2024-04-06.

* Fixed OmeDateField and Success Dialog

## [0.1.6] - 2024-04-06.

* Fixed OmegaEnumField

## [0.1.5] - 2024-04-06.

* Fixed onChanged event on OmegaMoneyField

## [0.1.4] - 2024-04-05.

* Added new events to OmegaNumericField

## [0.1.3] - 2024-04-03.

* Adjusted dialogs and CommonUtil class, create new dialog for warning and error messages detailed inside collpasible

## [0.1.2] - 2024-04-03.

* Added to OmegaButton options: `iconSize`, `textAlign`

## [0.1.1] - 2024-03-28.

* Fixed OmegaButton Icon
* Added option to OmegaEnumField to add Icon

## [0.1.0] - 2024-03-25.

* Fixed OmegaButton 

## [0.0.9] - 2023-11-23.

* Added ResponsiveFontSize Helper

## [0.0.8] - 2023-08-25.

* Bug fixed on search field to select value

## [0.0.7] - 2023-08-23.

* Addes property visible to field components

## [0.0.6] - 2023-06-01.

* Updated dependencies

## [0.0.5] - 2023-04-04.

* Upgrade to Flutter 3.7.x

## [0.0.4] - 2022-02-17.

* Fixed OmegaTextField custom text controller set

## [0.0.3] - 2022-01-24.

* Fixed money formatter formatMoney
* Fixed OmegaTextField setSelected

## [0.0.2] - 2021-12-20.

* Fixed WhitelistingTextInputFormatter, replaced by FilteringTextInputFormatter

## [0.0.1] - 2021-12-09.

* Initial version

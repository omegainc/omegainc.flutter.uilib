import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../omegalibconfig.dart';

class OmegaEnumData {
  String presentation;
  IconData? icon;
  Color? iconColor;
  dynamic value;

  OmegaEnumData({
    required this.presentation,
    this.value,
    this.icon,
    this.iconColor = Colors.black,
  });
}

// ignore: must_be_immutable
class OmegaEnumField extends StatefulWidget {
  List<OmegaEnumData> data = [];

  bool visible;
  bool enabled;
  Color labelColor = Colors.black;
  String? label = "";
  TextStyle? labelStyle;
  String? selected = "";
  bool showRequiredLabel = false;
  double labelSize = 14;

  String? Function(String?)? validator;
  Function(dynamic value, dynamic key)? onSaved;
  Function(dynamic value)? onSelect;

  OmegaEnumField({
    Key? key,
    this.enabled = true,
    this.visible = true,
    required this.data,
    this.label,
    this.labelSize = 14,
    this.labelColor = Colors.black,
    this.validator,
    this.onSaved,
    this.showRequiredLabel = false,
    this.selected,
    this.onSelect,
    this.labelStyle,
  }) : super(key: key);

  @override
  OmegaEnumFieldState createState() => OmegaEnumFieldState();
}

class OmegaEnumFieldState extends State<OmegaEnumField> {
  String? selectedText = null;
  List<OmegaEnumData> _itens = <OmegaEnumData>[];
  FormFieldState<String>? formState;

  @override
  void initState() {
    super.initState();

    setData(widget.data);

    WidgetsBinding.instance?.addPostFrameCallback((_) => setSelectedSate());
  }

  void setData(List<OmegaEnumData> data) {
    _itens.clear();

    setState(() {
      _itens = data;

      if (_itens.length > 0) selectedText = _itens.first.presentation;

      if (widget.selected != null && widget.selected!.isNotEmpty) {
        selectedText = widget.selected;
      }
    });
  }

  /**
   * Set the visibility of the field
   */
  void setVisibility(bool visible) {
    setState(() {
      widget.visible = visible;
    });
  }

  void setSelectedSate() {
    if (this.selectedText != null && this.selectedText!.isNotEmpty) {
      setState(() {
        formState?.didChange(this.selectedText);
      });

      if (widget.onSelect != null) {
        var key = _itens
                .where((item) => item.presentation == this.selectedText)
                .first
                .value ??
            null;

        widget.onSelect!(key);
      }
    }
  }

  void setSelected(String? selected) {
    setState(() {
      this.selectedText = selected;
      formState?.didChange(selected);
    });

    if (widget.onSelect != null) {
      var key = _itens
              .where((item) => item.presentation == this.selectedText)
              .first
              .value ??
          null;

      widget.onSelect!(key);
    }
  }

  void setSelectedByValue(dynamic value) {
    var item = _itens.where((w) => w.value == value).toList().first;
    setSelected(item.presentation);
  }

  Widget _buildPresentation(OmegaEnumData item) {
    if (item.icon != null) {
      return Row(
        children: <Widget>[
          Icon(item.icon, size: widget.labelSize, color: item.iconColor),
          SizedBox(width: 10),
          Text(
            item.presentation,
            style: TextStyle(
              color: Colors.black,
              fontSize: widget.labelSize,
            ),
          ),
        ],
      );
    } else {
      return Text(
        item.presentation,
        style: TextStyle(
          color: Colors.black,
          fontSize: widget.labelSize,
        ),
      );
    }
  }

  Widget getDropDown(FormFieldState<String> state) {
    formState = state;

    var items = _itens.map((item) {
      return new DropdownMenuItem<String>(
        value: item.presentation,
        child: _buildPresentation(item),
      );
    }).toList();

    return DropdownButton<String>(
      value: this.selectedText,
      isDense: true,
      iconEnabledColor: Colors.grey,
      dropdownColor: Colors.white,
      onChanged: (String? selected) => setSelected(selected),
      items: items,
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.visible
        ? Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 7, bottom: 5, top: 5),
                    child: Text(
                      widget.showRequiredLabel
                          ? widget.label! + ' *'
                          : widget.label!,
                      textAlign: TextAlign.left,
                      style: widget.labelStyle != null
                          ? widget.labelStyle
                          : TextStyle(
                              color: widget.labelColor,
                              fontSize: widget.labelSize,
                            ),
                    ),
                  )
                ],
              ),
              FormField<String>(
                enabled: widget.enabled,
                onSaved: (value) {
                  var key = _itens
                          .where((item) => item.presentation == value)
                          .first
                          .value ??
                      null;

                  if (widget.onSaved != null) {
                    widget.onSaved!(value, key);
                  }
                },
                validator: widget.validator,
                builder: (FormFieldState<String> state) {
                  return InputDecorator(
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.grey[200],
                      contentPadding: EdgeInsets.all(15.0),
                      border: InputBorder.none,
                      enabledBorder: OmegaLibConfig.enabledBorder,
                      focusedBorder: OmegaLibConfig.focusedBorder,
                      focusedErrorBorder: OmegaLibConfig.focusedErrorBorder,
                      errorBorder: OmegaLibConfig.errorBorder,
                      errorStyle: OmegaLibConfig.errorStyle,
                    ),
                    isEmpty: selectedText == '',
                    child: DropdownButtonHideUnderline(
                      child: getDropDown(state),
                    ),
                  );
                },
              )
            ],
          )
        : Container();
  }
}

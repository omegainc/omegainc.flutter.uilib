import 'package:flutter/material.dart';

// ignore: must_be_immutable
class OmegaGradientButton extends StatelessWidget {
  double width, height, fontSize, iconSpace;
  bool withIcon;
  IconData? icon;
  String text;
  void Function()? onPressed;
  Color color, iconColor, textColor;

  OmegaGradientButton({
    required this.text,
    this.icon,
    required this.width,
    required this.height,
    this.withIcon = false,
    this.fontSize = 14,
    this.iconSpace = 0,
    this.onPressed,
    this.color = Colors.black,
    this.iconColor = Colors.grey,
    this.textColor = Colors.white,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 2, bottom: 2),
      child: SizedBox(
        width: width,
        height: height,
        child: Material(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
              side: BorderSide(
                color: Colors.transparent,
              ),
          ),
          color: color,
          child: ElevatedButton(
            onPressed: onPressed,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                withIcon
                    ? Container(
                  child: Row(
                    children: <Widget>[
                      Icon(
                        icon,
                        color: iconColor,
                      ),
                      SizedBox(
                        width: iconSpace,
                      ),
                    ],
                  ),
                )
                    : Container(),
                Text(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: textColor,
                    fontSize: fontSize,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:omegainc_lib/util/datetime.util.dart';
import 'package:omegainc_lib/util/mask.util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../omegalibconfig.dart';

// ignore: must_be_immutable
class OmegaTimeField extends StatefulWidget {
  bool enabled;
  bool visible;
  Color labelColor = Colors.black;
  Color iconColor;
  bool showRequiredLabel = false;
  bool autofocus = false;
  String label = "";
  double labelSize = 14;
  String? hint = "";
  bool obscureText = false;
  bool showPassButton = false;
  TimeOfDay? initialValue;

  String? Function(String?)? validator;
  Function(String)? onChanged;
  Function(String?)? onSaved;
  Function()? onTap;

  Function(dynamic)? onSelect;

  FocusNode? focusNode = new FocusNode();

  OmegaTimeField({
    Key? key,
    this.enabled = true,
    this.visible = true,
    this.label = '',
    this.labelSize = 14,
    this.labelColor = Colors.black,
    this.iconColor = Colors.grey,
    this.hint,
    this.onChanged,
    this.onSaved,
    this.onTap,
    this.focusNode,
    this.validator,
    this.showRequiredLabel = false,
    this.autofocus = false,
    this.initialValue,
    this.onSelect,
  }) : super(key: key);

  @override
  OmegaTimeFieldState createState() => OmegaTimeFieldState();
}

class OmegaTimeFieldState extends State<OmegaTimeField> {
  TimeOfDay? selectedTime = TimeOfDay.now();

  final TextEditingController _controller = new TextEditingController();

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) => _initialize());
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _initialize() async {
    if (widget.initialValue != null) {
      this.setSelected(widget.initialValue!);
    }
  }

  /**
   * Set the visibility of the field
   */
  void setVisibility(bool visible) {
    setState(() {
      widget.visible = visible;
    });
  }

  Future<Null> _selectTime() async {
    final TimeOfDay? picked = await showTimePicker(
      context: this.context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != null) setSelected(picked);
  }

  void setSelected(TimeOfDay selected) {
    setState(() {
      selectedTime = selected;

      _controller.text = DateTimeUtil.formatTimeOfDay(selected);
    });

    if (widget.onSelect != null) {
      widget.onSelect!(selected);
    }
  }

  void clearSelection() {
    setState(() {
      selectedTime = null;
      _controller.text = '';
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.visible
        ? Container(
            padding: EdgeInsets.only(top: 4, bottom: 4),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 7, bottom: 5, top: 5),
                      child: Text(
                        widget.showRequiredLabel
                            ? widget.label + ' *'
                            : widget.label,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: widget.labelColor,
                            fontSize: widget.labelSize),
                      ),
                    )
                  ],
                ),
                TextFormField(
                  style: TextStyle(
                    color: widget.enabled ? Colors.black : Colors.grey,
                    fontSize: widget.labelSize,
                  ),
                  textCapitalization: TextCapitalization.none,
                  autofocus: widget.autofocus,
                  controller: _controller,
                  inputFormatters: [
                    MaskUtil().maskHora,
                  ],
                  enabled: widget.enabled,
                  focusNode: widget.focusNode,
                  onChanged: widget.onChanged,
                  onSaved: widget.onSaved,
                  onTap: widget.onTap,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintText: widget.hint,
                    hintStyle: TextStyle(color: Colors.grey),
                    suffixIcon: IconButton(
                      color: widget.iconColor,
                      icon: Icon(Icons.access_time_outlined),
                      onPressed: () => _selectTime(),
                    ),
                    contentPadding: EdgeInsets.all(15.0),
                    border: InputBorder.none,
                    enabledBorder: OmegaLibConfig.enabledBorder,
                    focusedBorder: OmegaLibConfig.focusedBorder,
                    focusedErrorBorder: OmegaLibConfig.focusedErrorBorder,
                    errorBorder: OmegaLibConfig.errorBorder,
                    errorStyle: OmegaLibConfig.errorStyle,
                    disabledBorder: OmegaLibConfig.disabledBorder,
                  ),
                  obscureText: widget.obscureText,
                  validator: widget.validator,
                ),
              ],
            ),
          )
        : Container();
  }
}

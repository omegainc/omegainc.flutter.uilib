import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_password_strength/flutter_password_strength.dart';

import '../../omegalibconfig.dart';

// ignore: must_be_immutable
class OmegaPasswordField extends StatefulWidget {
  Color labelColor = Colors.black;
  String label = "";
  double labelSize = 14;

  bool visible;
  bool enabled;

  TextStyle? labelStyle;

  bool showRequiredLabel = false;
  bool autofocus = false;
  String? hint = "";
  bool autoCorrect = false;
  bool passwordStrength;
  int? maxLength;
  String? initialValue;
  Widget? prefix;

  String? Function(String?)? validator;
  Function(String)? onChanged;
  Function(String?)? onSaved;
  Function()? onTap;
  Function(double)? strengthCallback;
  FocusNode? focusNode = new FocusNode();

  OmegaPasswordField({
    Key? key,
    this.enabled = true,
    this.visible = true,
    this.label = '',
    this.labelSize = 14,
    this.labelColor = Colors.black,
    this.hint,
    this.maxLength,
    this.onChanged,
    this.onSaved,
    this.onTap,
    this.focusNode,
    this.validator,
    this.showRequiredLabel = false,
    this.autofocus = false,
    this.initialValue,
    this.autoCorrect = false,
    this.prefix,
    this.labelStyle,
    this.passwordStrength = false,
    this.strengthCallback,
  }) : super(key: key);

  @override
  OmegaPasswordFieldState createState() => OmegaPasswordFieldState();
}

class OmegaPasswordFieldState extends State<OmegaPasswordField> {
  final TextEditingController _controller = new TextEditingController();
  bool _obscureText = true;
  String? selectedValue;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) => _initialize());
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _initialize() async {
    if (widget.initialValue != null) {
      this.setSelected(widget.initialValue!);
    }
  }

  /**
   * Set the visibility of the field
   */
  void setVisibility(bool visible) {
    setState(() {
      widget.visible = visible;
    });
  }

  void setSelected(String selected) {
    setState(() {
      _controller.text = selected;
      selectedValue = selected;
    });
  }

  void clearSelection() {
    setSelected('');
  }

  @override
  Widget build(BuildContext context) {
    return widget.visible
        ? Container(
            padding: EdgeInsets.only(top: 4, bottom: 4),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 7, bottom: 5, top: 5),
                      child: Text(
                        widget.showRequiredLabel
                            ? widget.label + ' *'
                            : widget.label,
                        textAlign: TextAlign.left,
                        style: widget.labelStyle != null
                            ? widget.labelStyle
                            : TextStyle(
                                color: widget.labelColor,
                                fontSize: widget.labelSize,
                              ),
                      ),
                    )
                  ],
                ),
                TextFormField(
                  style: TextStyle(
                    color: widget.enabled ? Colors.black : Colors.grey,
                    fontSize: widget.labelSize,
                  ),
                  enabled: widget.enabled,
                  autocorrect: widget.autoCorrect,
                  maxLength: widget.maxLength,
                  autofocus: widget.autofocus,
                  controller: _controller,
                  focusNode: widget.focusNode,
                  onChanged: (value) {
                    if (widget.onChanged != null) {
                      widget.onChanged!(value);
                    }

                    setState(() {
                      selectedValue = value;
                    });
                  },
                  onSaved: widget.onSaved,
                  onTap: widget.onTap,
                  keyboardType: TextInputType.visiblePassword,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintText: widget.hint,
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    prefixIcon: widget.prefix,
                    suffixIcon: IconButton(
                      color: Colors.grey,
                      icon: Icon(_obscureText
                          ? Icons.visibility
                          : Icons.visibility_off),
                      onPressed: () {
                        setState(() {
                          _obscureText = _obscureText ? false : true;
                        });
                      },
                    ),
                    contentPadding: EdgeInsets.all(15.0),
                    border: InputBorder.none,
                    enabledBorder: OmegaLibConfig.enabledBorder,
                    focusedBorder: OmegaLibConfig.focusedBorder,
                    focusedErrorBorder: OmegaLibConfig.focusedErrorBorder,
                    errorBorder: OmegaLibConfig.errorBorder,
                    errorStyle: OmegaLibConfig.errorStyle,
                  ),
                  obscureText: _obscureText,
                  validator: widget.validator,
                ),
                widget.passwordStrength
                    ? Container(
                        padding: EdgeInsets.only(
                          top: 10,
                          bottom: 10,
                          left: 5,
                          right: 5,
                        ),
                        child: FlutterPasswordStrength(
                          password: selectedValue,
                          radius: 2.0,
                          strengthCallback: (strength) {
                            if (widget.passwordStrength &&
                                widget.strengthCallback != null) {
                              widget.strengthCallback!(strength);
                            }
                          },
                        ),
                      )
                    : SizedBox(height: 0.1),
              ],
            ),
          )
        : Container();
  }
}

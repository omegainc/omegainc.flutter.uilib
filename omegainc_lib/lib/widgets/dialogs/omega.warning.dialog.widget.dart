import 'package:omegainc_lib/consts/consts.dart';
import 'package:flutter/material.dart';

import '../../responsive.dart';

// ignore: must_be_immutable
class OmegaWarningDialogWidget extends StatefulWidget {
  String? description;
  String? title;
  Color? bgColor, textColor;

  OmegaWarningDialogWidget({
    required this.description,
    this.title = 'Atenção!',
    required this.textColor,
    required this.bgColor,
  });

  @override
  _OmegaWarningDialogWidgetState createState() =>
      _OmegaWarningDialogWidgetState();
}

class _OmegaWarningDialogWidgetState extends State<OmegaWarningDialogWidget> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  double _definePercentWidtSize() {
    double screenWidth = MediaQuery.of(context).size.width;

    if (screenWidth >= 2500) {
      return 0.20;
    } else if (Responsive.isDesktop(context)) {
      return 0.40;
    } else if (Responsive.isTablet(context)) {
      return 0.60;
    } else {
      return 0.85;
    }
  }

  dialogContent(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return Stack(
      children: <Widget>[
        Container(
          width: (screenWidth * _definePercentWidtSize()),
          padding: EdgeInsets.only(
            top: Consts.avatarRadius_Dialog + Consts.padding_Dialog,
            bottom: Consts.padding_Dialog,
            left: Consts.padding_Dialog,
            right: Consts.padding_Dialog,
          ),
          margin: EdgeInsets.only(top: Consts.avatarRadius_Dialog),
          decoration: new BoxDecoration(
            color: widget.bgColor,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(Consts.padding_Dialog),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              Text(
                widget.title ?? '',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.w700,
                  color: widget.textColor,
                ),
              ),
              SizedBox(height: 16.0),
              Container(
                child: SingleChildScrollView(
                  child: Text(
                    widget.description ?? '',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16.0, color: widget.textColor),
                  ),
                ),
              ),
              SizedBox(height: 24.0),
              Flexible(
                child: Container(
                  height: 40,
                  child: Row(
                    children: <Widget>[
                      Spacer(),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            'OK',
                            style: TextStyle(color: widget.textColor),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          top: Consts.padding_Dialog,
          left: Consts.padding_Dialog,
          right: Consts.padding_Dialog,
          child: SizedBox(
            height: 104,
            width: 104,
            child: CircleAvatar(
              backgroundColor: Colors.orange,
              radius: Consts.avatarRadius_Dialog,
              child: Icon(
                Icons.warning,
                color: Colors.white,
                size: 62,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

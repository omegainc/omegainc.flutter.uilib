import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:omegainc_lib/omegainc_lib.dart';

class StartView extends StatefulWidget {
  @override
  _StartViewState createState() => _StartViewState();
}

class _StartViewState extends State<StartView> {
  bool loading = false;
  String loadingMessage = "Aguarde...";
  CommonUtil commonUtil = CommonUtil();

  final _keyBanco = GlobalKey<OmegaSearchFieldState>();

  static List<OmegaEnumData> formasPagamento = [
    OmegaEnumData(
      icon: Icons.money,
      iconColor: Colors.green,
      presentation: 'Dinheiro',
      value: 0,
    ),
    OmegaEnumData(
      icon: Icons.credit_card,
      iconColor: Colors.blue,
      presentation: 'Cartão Crédito/Débito',
      value: 1,
    ),
  ];

  static List<OmegaEnumData> sexos = [
    OmegaEnumData(
      presentation: 'Masculino',
      value: 0,
    ),
    OmegaEnumData(
      presentation: 'Feminino',
      value: 1,
    ),
  ];

  static Future<List<SearchFieldSource>> getSourceBanco() async {
    var entities = <SearchFieldSource>[];

    try {
      for (var i = 0; i < 10; i++) {
        entities.add(new SearchFieldSource(
          label: 'TESTE BANCO $i',
          value: i,
        ));
      }
    } catch (error) {
      rethrow;
    }

    return entities;
  }

  @override
  Widget build(BuildContext context) {
    return LoadingOverlay(
      color: Colors.grey,
      isLoading: loading,
      progressIndicator: OmegaLoadingWidget(
        textColor: Colors.black,
        message: loadingMessage,
      ),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: OmegaAppBar(
          brightness: Brightness.dark,
          bgColor: Colors.blue,
          iconDataColor: Colors.white,
          textColor: Colors.white,
          titleSize: 17,
          title: 'Lib Test',
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(10.0),
            child: Column(
              children: [
                Container(
                  color: Colors.grey,
                  height: 350,
                  child: Stack(
                    children: [
                      Positioned(
                        top: 10.0,
                        left: 10,
                        right: 10,
                        child: Container(
                          color: Colors.red,
                          height: 90,
                        ),
                      ),
                      Positioned(
                        top: 25,
                        left: 20,
                        right: 10,
                        child: Container(
                          color: Colors.yellow,
                          height: 90,
                        ),
                      ),
                    ],
                  ),
                ),
                Text(
                  'Teste',
                  style: TextStyle(
                    fontSize: ResponsiveFontSizeHelper.sp(context, 22),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                OmegaDateField(
                  label: 'Data',
                  enabled: true,
                  labelSize: ResponsiveFontSizeHelper.sp(context, 15),
                  showRequiredLabel: true,
                  onSelect: (value) {
                    print('Mudou $value');
                  },
                ),
                OmegaSearchField(
                  key: _keyBanco,
                  colorWhite: Colors.black,
                  getSource: () async => await getSourceBanco(),
                  barText: 'Buscar Banco',
                  label: 'Banco',
                  labelSize: ResponsiveFontSizeHelper.sp(context, 15),
                  hint: 'Selecione...',
                  keyboardType: TextInputType.text,
                  showRequiredLabel: true,
                  onSaved: (value, result) {},
                  colorIcon: Colors.green,
                  colorPrimary: Colors.blueGrey,
                ),
                OmegaTextField(
                  initialValue: 'Teste',
                  labelSize: ResponsiveFontSizeHelper.sp(context, 15),
                  label: 'Teste campo de texto',
                ),
                OmegaTextField(
                  label: 'Teste campo de texto com max length',
                  maxLength: 5,
                ),
                OmegaTextField(
                  prefix: IconButton(
                    icon: Icon(Icons.close),
                    onPressed: () {},
                  ),
                  label: 'Teste campo com prefixo',
                ),
                OmegaEnumField(
                  visible: true,
                  data: formasPagamento,
                  label: 'Forma de Pagamento',
                  labelSize: ResponsiveFontSizeHelper.sp(context, 18),
                  onSelect: (value) {
                    print('Mudou $value');
                  },
                ),
                OmegaEnumField(
                  data: sexos,
                  label: 'Sexo',
                ),
                OmegaTextArea(
                  label: 'Text Area',
                ),
                Container(
                  margin: EdgeInsets.all(15.0),
                  child: OmegaButton(
                    borderRadius: 10.0,
                    color: Colors.deepOrange,
                    textColor: Colors.black,
                    height: 50.0,
                    text: 'Teste',
                    onPressed: () async {
                      setState(() {
                        _keyBanco.currentState?.setSelectedById(3);
                      });
                    },
                  ),
                ),
                OmegaButton(
                  borderRadius: 10.0,
                  height: 50.0,
                  fontSize: 25,
                  icon: Icons.add,
                  iconSize: 50,
                  iconSpace: 10,
                  iconColor: Colors.white,
                  text: 'Teste icon size e font size',
                  onPressed: () async {
                    setState(() {
                      _keyBanco.currentState?.setSelectedById(3);
                    });
                  },
                ),
                Text(
                  'Dialogs',
                  style: TextStyle(
                    fontSize: ResponsiveFontSizeHelper.sp(context, 22),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Divider(),
                OmegaButton(
                  borderRadius: 10.0,
                  height: 50.0,
                  text: 'Info Dialog',
                  onPressed: () async {
                    commonUtil.info(context, 'Mensagem de informação');
                  },
                ),
                OmegaButton(
                  borderRadius: 10.0,
                  height: 50.0,
                  text: 'Error Dialog',
                  onPressed: () async {
                    commonUtil.errorException(context, 'Mensagem de erro');
                  },
                ),
                OmegaButton(
                  borderRadius: 10.0,
                  height: 50.0,
                  text: 'New Error Dialog',
                  onPressed: () async {
                    commonUtil.error(context, 'Mensagem de erro', Exception('Houve um erro gerado por simulacao e nao foi possivel realizar a operacao por favor tente novamente mais tarde.'));
                  },
                ),
                OmegaButton(
                  borderRadius: 10.0,
                  height: 50.0,
                  text: 'Success Dialog',
                  onPressed: () async {
                    commonUtil.success(context, 'Mensagem de sucesso');
                  },
                ),
                OmegaButton(
                  borderRadius: 10.0,
                  height: 50.0,
                  text: 'Warning Dialog',
                  onPressed: () async {
                    commonUtil.warning(context, 'Mensagem de alerta');
                  },
                ),
                OmegaButton(
                  borderRadius: 10.0,
                  height: 50.0,
                  text: 'Confirm Dialog',
                  onPressed: () async {
                    commonUtil.confirm(
                      context,
                      'Confirmer',
                      'Mensagem de confirmação',
                      () {
                        Navigator.pop(context);
                        commonUtil.toastInfo('Confirmado', context);
                      },
                      () {
                        Navigator.pop(context);
                        commonUtil.toastInfo('Cancelado', context);
                      },
                    );
                  },
                ),
                OmegaButton(
                  borderRadius: 10.0,
                  height: 50.0,
                  text: 'Loading Dialog',
                  onPressed: () async {
                    commonUtil.loading(
                        context, 'Aguarde!', 'Carregando dados...', () async {
                      Future.delayed(Duration(seconds: 5), () {
                        Navigator.pop(context);
                      });
                    });
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

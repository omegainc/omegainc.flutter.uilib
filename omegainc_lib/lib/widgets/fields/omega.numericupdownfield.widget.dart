import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../omegalibconfig.dart';

// ignore: must_be_immutable
class OmegaNumericUpDownField extends StatefulWidget {
  bool visible;
  bool enabled;
  Color labelColor = Colors.black;
  bool showRequiredLabel = false;
  int minValue = 1;
  bool autofocus = false;
  String? label = "";
  double labelSize = 14;
  String? hint = "";
  int? initialValue;
  Color? iconColor;

  String? Function(int?)? validator;
  Function(int)? onChanged;
  Function(int?)? onSaved;
  Function()? onTap;
  FocusNode? focusNode = new FocusNode();

  OmegaNumericUpDownField({
    Key? key,
    this.enabled = false,
    this.visible = true,
    this.label = '',
    this.labelSize = 14,
    this.labelColor = Colors.black,
    this.hint,
    this.onChanged,
    this.onSaved,
    this.onTap,
    this.focusNode,
    this.validator,
    this.showRequiredLabel = false,
    this.autofocus = false,
    this.iconColor = Colors.grey,
    this.initialValue,
    this.minValue = 1,
  }) : super(key: key);

  @override
  OmegaNumericUpDownFieldState createState() => OmegaNumericUpDownFieldState();
}

class OmegaNumericUpDownFieldState extends State<OmegaNumericUpDownField> {
  final TextEditingController _controller = new TextEditingController();
  int? selectedValue;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) => _initialize());
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _initialize() async {
    if (widget.initialValue != null) {
      this.setSelected(widget.initialValue!);
    }
  }

  /**
   * Set the visibility of the field
   */
  void setVisibility(bool visible) {
    setState(() {
      widget.visible = visible;
    });
  }

  void setSelected(int selected) {
    if (selected >= widget.minValue) {
      setState(() {
        selectedValue = selected;
        _controller.text = selected.toString();
      });

      if (widget.onChanged != null) {
        widget.onChanged!(selected);
      }
    }
  }

  void clearSelection() {
    setState(() {
      selectedValue = null;
      _controller.text = '';
    });
  }

  int _convertValue(String? value) {
    if (value == null) return 0;

    String _onlyDigits = value.replaceAll(RegExp('[^0-9]'), "");
    _onlyDigits = _onlyDigits.isEmpty ? "0" : _onlyDigits;

    int _intValue = int.tryParse(_onlyDigits) ?? 0;

    return _intValue;
  }

  @override
  Widget build(BuildContext context) {
    return widget.visible
        ? Container(
            padding: EdgeInsets.only(top: 4, bottom: 4),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 7, bottom: 5, top: 5),
                      child: Text(
                        widget.showRequiredLabel
                            ? widget.label! + ' *'
                            : widget.label!,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: widget.labelColor,
                            fontSize: widget.labelSize),
                      ),
                    )
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 3,
                      child: TextButton(
                        child: Icon(
                          Icons.arrow_upward,
                          color: widget.iconColor,
                        ),
                        onPressed: () => setSelected(selectedValue ?? 0 + 1),
                      ),
                    ),
                    Flexible(
                      flex: 3,
                      child: TextFormField(
                        textAlign: TextAlign.center,
                        enabled: false,
                        style: TextStyle(
                          color: widget.enabled ? Colors.black : Colors.grey,
                          fontSize: widget.labelSize,
                        ),
                        textCapitalization: TextCapitalization.none,
                        autofocus: widget.autofocus,
                        controller: _controller,
                        focusNode: widget.focusNode,
                        onChanged: (value) {
                          setState(() {
                            selectedValue = int.tryParse(value);
                          });

                          if (widget.onChanged != null) {
                            widget.onChanged!(int.tryParse(value) ?? 0);
                          }
                        },
                        onSaved: (value) {
                          widget.onSaved!(_convertValue(value));
                        },
                        onTap: widget.onTap,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.grey[200],
                          hintText: widget.hint,
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          ),
                          contentPadding: EdgeInsets.all(1.0),
                          border: InputBorder.none,
                          enabledBorder: OmegaLibConfig.enabledBorder,
                          focusedBorder: OmegaLibConfig.focusedBorder,
                          focusedErrorBorder: OmegaLibConfig.focusedErrorBorder,
                          errorBorder: OmegaLibConfig.errorBorder,
                          errorStyle: OmegaLibConfig.errorStyle,
                        ),
                        validator: (value) {
                          if (widget.validator == null) {
                            return null;
                          } else {
                            return widget.validator!(_convertValue(value));
                          }
                        },
                      ),
                    ),
                    Flexible(
                      flex: 3,
                      child: TextButton(
                        child: Icon(
                          Icons.arrow_downward,
                          color: widget.iconColor,
                        ),
                        onPressed: () => setSelected(selectedValue ?? 1 - 1),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        : Container();
  }
}

import 'package:flutter/material.dart';

class OmegaButtonSocialWidget extends StatelessWidget {
  IconData icon;
  Color foreGroundColor, backGroundColor;
  void Function()? onPressed;

  OmegaButtonSocialWidget({
    required this.icon,
    required this.foreGroundColor,
    required this.backGroundColor,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10),
      padding: EdgeInsets.all(10),
      child: SizedBox(
        width: 50,
        height: 50,
        child: Material(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
          ),
          textStyle: TextStyle(color: foreGroundColor),
          color: backGroundColor,
          child: TextButton(
            child: Icon(icon),
            onPressed: onPressed,
          ),
        ),
      ),
    );
  }
}

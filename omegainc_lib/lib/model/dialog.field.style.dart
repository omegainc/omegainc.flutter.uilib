import 'package:flutter/material.dart';

class DialogFieldStyle {
  Color textColor;
  Color bgColor;

  DialogFieldStyle({required this.textColor, required this.bgColor});
}

import 'package:flutter/material.dart';

class OmegaCenterFloatingWidget extends StatefulWidget {
  Color color;
  IconData icon;
  String label;
  void Function()? onPressed;

  OmegaCenterFloatingWidget({
    required this.color,
    required this.label,
    required this.icon,
    this.onPressed,
  });

  @override
  _OmegaCenterFloatingWidgetState createState() =>
      _OmegaCenterFloatingWidgetState();
}

class _OmegaCenterFloatingWidgetState extends State<OmegaCenterFloatingWidget> {
  @override
  Widget build(BuildContext context) {
    bool keyboardIsOpened = MediaQuery.of(context).viewInsets.bottom != 0.0;

    return Container(
      padding: (keyboardIsOpened)
          ? EdgeInsets.only(bottom: 55)
          : EdgeInsets.only(bottom: 5),
      child: FloatingActionButton.extended(
        backgroundColor: widget.color,
        elevation: 4.0,
        icon: Icon(widget.icon),
        label: Text(
          widget.label,
          style: TextStyle(fontSize: 16),
        ),
        onPressed: widget.onPressed,
      ),
    );
  }
}

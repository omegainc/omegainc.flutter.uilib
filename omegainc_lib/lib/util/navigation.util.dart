import 'package:flutter/material.dart';

class NavigationUtil {
  static Future<dynamic> navigateTo(BuildContext context, Widget page) async {
    return await Navigator.push(
        context, new MaterialPageRoute(builder: (ctx) => page));
  }

  static navigateRemoveUntil(BuildContext context, Widget page) {
    Navigator.pushAndRemoveUntil(
      context,
      PageRouteBuilder(pageBuilder: (context, _, __) => page),
      (Route<dynamic> r) => false,
    );
  }

  static Future<dynamic> navigateRoute(
      BuildContext context, String route) async {
    return await Navigator.pushNamed(context, route);
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../omegalibconfig.dart';

// ignore: must_be_immutable
class OmegaTextArea extends StatefulWidget {
  bool visible;
  bool enabled;
  Color labelColor = Colors.black;
  bool showRequiredLabel = false;
  bool autofocus = false;
  String label = "";
  double labelSize = 14;
  String? hint = "";
  int? maxLength, minLines, maxLines;
  TextInputType keyboardType = TextInputType.text;
  TextCapitalization textCapitalization = TextCapitalization.none;
  String? initialValue;

  TextStyle? labelStyle;

  String? Function(String?)? validator;
  Function(String)? onChanged;
  Function(String?)? onSaved;
  Function()? onTap;
  FocusNode? focusNode = new FocusNode();

  List<TextInputFormatter>? inputFormatters = <TextInputFormatter>[];

  OmegaTextArea({
    Key? key,
    this.enabled = true,
    this.visible = true,
    this.label = '',
    this.labelSize = 14,
    this.labelColor = Colors.black,
    this.hint,
    this.maxLength,
    this.keyboardType = TextInputType.text,
    this.onChanged,
    this.onSaved,
    this.onTap,
    this.focusNode,
    this.inputFormatters,
    this.validator,
    this.showRequiredLabel = false,
    this.autofocus = false,
    this.textCapitalization = TextCapitalization.none,
    this.initialValue,
    this.minLines = 2,
    this.maxLines = 10,
    this.labelStyle,
  }) : super(key: key);

  @override
  OmegaTextAreaState createState() => OmegaTextAreaState();
}

class OmegaTextAreaState extends State<OmegaTextArea> {
  final TextEditingController _controller = new TextEditingController();

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) => _initialize());
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _initialize() async {
    if (widget.initialValue != null) {
      this.setSelected(widget.initialValue!);
    }
  }

  /**
   * Set the visibility of the field
   */
  void setVisibility(bool visible) {
    setState(() {
      widget.visible = visible;
    });
  }

  void setSelected(String selected) {
    setState(() {
      _controller.text = selected;
    });
  }

  void clearSelection() {
    setState(() {
      _controller.text = '';
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.visible
        ? Container(
            padding: EdgeInsets.only(top: 4, bottom: 4),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 7, bottom: 5, top: 5),
                      child: Text(
                        widget.showRequiredLabel
                            ? widget.label + ' *'
                            : widget.label,
                        textAlign: TextAlign.left,
                        style: widget.labelStyle != null
                            ? widget.labelStyle
                            : TextStyle(
                                color: widget.labelColor,
                                fontSize: widget.labelSize),
                      ),
                    )
                  ],
                ),
                TextFormField(
                  style: TextStyle(
                    color: widget.enabled ? Colors.black : Colors.grey,
                    fontSize: widget.labelSize,
                  ),
                  enabled: widget.enabled,
                  minLines: widget.minLines,
                  maxLines: widget.maxLines,
                  autocorrect: false,
                  maxLength: widget.maxLength,
                  textCapitalization: widget.textCapitalization,
                  autofocus: widget.autofocus,
                  controller: _controller,
                  inputFormatters: widget.inputFormatters,
                  focusNode: widget.focusNode,
                  onChanged: widget.onChanged,
                  onSaved: widget.onSaved,
                  onTap: widget.onTap,
                  keyboardType: widget.keyboardType,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintText: widget.hint,
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    contentPadding: EdgeInsets.all(15.0),
                    border: InputBorder.none,
                    enabledBorder: OmegaLibConfig.enabledBorder,
                    focusedBorder: OmegaLibConfig.focusedBorder,
                    focusedErrorBorder: OmegaLibConfig.focusedErrorBorder,
                    errorBorder: OmegaLibConfig.errorBorder,
                    errorStyle: OmegaLibConfig.errorStyle,
                  ),
                  validator: widget.validator,
                )
              ],
            ),
          )
        : Container();
  }
}

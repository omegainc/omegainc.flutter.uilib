import 'package:flutter/material.dart';
import 'package:omegainc_lib/model/dialog.field.style.dart';

class OmegaLibConfig {
  static InputBorder enabledBorder = OutlineInputBorder(
    borderSide: BorderSide(color: Colors.transparent, width: 0.0),
    borderRadius: const BorderRadius.all(const Radius.circular(10.5)),
  );

  static InputBorder disabledBorder = OutlineInputBorder(
    borderSide: BorderSide(color: Colors.transparent, width: 0.0),
    borderRadius: const BorderRadius.all(const Radius.circular(10.5)),
  );

  static InputBorder focusedBorder = OutlineInputBorder(
    borderSide: BorderSide(color: Colors.transparent, width: 0.0),
    borderRadius: const BorderRadius.all(const Radius.circular(10.5)),
  );

  static InputBorder errorBorder = OutlineInputBorder(
    borderSide: BorderSide(color: Colors.red, width: 0.0),
    borderRadius: const BorderRadius.all(const Radius.circular(10.5)),
  );

  static InputBorder outlineInputBorder = OutlineInputBorder(
    borderSide: BorderSide(color: Colors.red, width: 0.0),
    borderRadius: const BorderRadius.all(const Radius.circular(10.5)),
  );

  static InputBorder focusedErrorBorder = OutlineInputBorder(
    borderSide: BorderSide(color: Colors.orangeAccent, width: 0.0),
    borderRadius: const BorderRadius.all(const Radius.circular(10.5)),
  );

  static TextStyle errorStyle = TextStyle(
    color: Color(0xFFe74c3c),
  );

  static DialogFieldStyle dialogFieldStyle = DialogFieldStyle(
    textColor: Colors.black,
    bgColor: Colors.white,
  );

  /**
   * Utilizado para o envio de report de bugs, quando preenchido é exibido o botão reportar erro em error dialogs
   */
  static String? emailToReport;
}

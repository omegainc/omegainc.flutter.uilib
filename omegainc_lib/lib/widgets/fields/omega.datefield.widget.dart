import 'package:omegainc_lib/util/mask.util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import '../../omegalibconfig.dart';

// ignore: must_be_immutable
class OmegaDateField extends StatefulWidget {
  bool visible;
  bool enabled;
  Color? labelColor = Colors.black;
  Color? iconColor;
  bool showRequiredLabel = false;
  bool autofocus = false;
  String? label = "";
  TextStyle? labelStyle;
  double labelSize = 14;
  String? hint = "";
  bool? obscureText = false;
  bool? showPassButton = false;
  DateTime? initialValue;

  String? Function(String?)? validator;
  Function(String)? onChanged;
  Function(String?)? onSaved;
  Function()? onTap;
  Function(dynamic)? onSelect;
  FocusNode? focusNode = new FocusNode();

  OmegaDateField({
    Key? key,
    this.enabled = true,
    this.visible = true,
    this.label,
    this.labelSize = 14,
    this.labelColor = Colors.black,
    this.labelStyle,
    this.iconColor,
    this.hint,
    this.onChanged,
    this.onSaved,
    this.onTap,
    this.focusNode,
    this.validator,
    this.showRequiredLabel = false,
    this.autofocus = false,
    this.initialValue,
    this.onSelect,
  }) : super(key: key);

  @override
  OmegaDateFieldState createState() => OmegaDateFieldState();
}

class OmegaDateFieldState extends State<OmegaDateField> {
  DateTime? selectedDate;

  final TextEditingController _controller = new TextEditingController();

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) => _initialize());
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _initialize() async {
    if (widget.initialValue != null) {
      this.setSelected(widget.initialValue!);
    }
  }

  /**
   * Set the visibility of the field
   */
  void setVisibility(bool visible) {
    setState(() {
      widget.visible = visible;
    });
  }

  Future<Null> _selectDate() async {
    final DateTime? picked = await showDatePicker(
        locale: Locale('pt'),
        context: this.context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900, 8),
        lastDate: DateTime(2201));

    if (picked != null && picked != selectedDate) {
      setSelected(picked);
    }
  }

  void setSelected(DateTime selected, {bool triggerEvents = true}) {
    String textDate = new DateFormat('dd/MM/yyyy').format(selected);

    setState(() {
      selectedDate = selected;
      _controller.text = textDate;
    });

    if (triggerEvents) {
      if (widget.onChanged != null) {
        widget.onChanged!(textDate);
      }

      if (widget.onSelect != null) {
        widget.onSelect!(selected);
      }
    }
  }

  void clearSelection() {
    setState(() {
      selectedDate = null;
      _controller.text = '';
    });
  }

  @override
  Widget build(BuildContext context) {
    bool showRequiredLabel = widget.showRequiredLabel;
    String label = widget.label ?? '';
    label = showRequiredLabel ? label + ' *' : label;

    return Visibility(
      visible: widget.visible,
      child: InkWell(
        onTap: () {
          if (widget.enabled) {
            _selectDate();
          }
        },
        child: Container(
          padding: EdgeInsets.only(top: 4, bottom: 4),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 7, bottom: 5, top: 5),
                    child: Text(
                      label,
                      textAlign: TextAlign.left,
                      style: widget.labelStyle != null
                          ? widget.labelStyle
                          : TextStyle(
                        color: widget.labelColor,
                        fontSize: widget.labelSize,
                      ),
                    ),
                  )
                ],
              ),
              TextFormField(
                style: TextStyle(
                  color: widget.enabled ? Colors.black : Colors.grey,
                  fontSize: widget.labelSize,
                ),
                enabled: widget.enabled,
                textCapitalization: TextCapitalization.none,
                autofocus: widget.autofocus,
                controller: _controller,
                inputFormatters: [
                  MaskUtil().maskData,
                ],
                focusNode: widget.focusNode,
                onChanged: widget.onChanged,
                onSaved: widget.onSaved,
                onTap: widget.onTap,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.grey[200],
                  hintText: widget.hint,
                  hintStyle: TextStyle(
                    color: Colors.grey,
                  ),
                  suffixIcon: IconButton(
                    color: widget.iconColor,
                    icon: Icon(Icons.calendar_today_outlined),
                    onPressed: () => _selectDate(),
                  ),
                  contentPadding: EdgeInsets.all(15.0),
                  border: InputBorder.none,
                  enabledBorder: OmegaLibConfig.enabledBorder,
                  focusedBorder: OmegaLibConfig.focusedBorder,
                  focusedErrorBorder: OmegaLibConfig.focusedErrorBorder,
                  errorBorder: OmegaLibConfig.errorBorder,
                  errorStyle: OmegaLibConfig.errorStyle,
                ),
                obscureText: widget.obscureText ?? false,
                validator: widget.validator,
              )
            ],
          ),
        ),
      ),
    );
  }
}

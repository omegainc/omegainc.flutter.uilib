import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../omegalibconfig.dart';

class OmegaTextField extends StatefulWidget {
  Color labelColor = Colors.black;
  String label = "";
  double labelSize = 14;
  bool visible;
  bool enabled;

  TextStyle? labelStyle;

  bool showRequiredLabel = false;
  bool autofocus = false;
  String? hint = "";
  bool obscureText = false;
  bool showPassButton = false;
  bool autoCorrect = false;
  int? maxLength;
  TextInputType keyboardType = TextInputType.text;
  TextCapitalization textCapitalization = TextCapitalization.none;
  String? initialValue;
  Widget? prefix;
  TextEditingController? textController;

  String? Function(String?)? validator;
  Function(String)? onChanged;
  Function(String?)? onSaved;
  Function()? onTap;

  FocusNode? focusNode = new FocusNode();

  List<TextInputFormatter>? inputFormatters = <TextInputFormatter>[];

  OmegaTextField({
    Key? key,
    this.enabled = true,
    this.visible = true,
    this.label = '',
    this.labelSize = 14,
    this.labelColor = Colors.black,
    this.hint,
    this.maxLength,
    this.obscureText = false,
    this.showPassButton = false,
    this.keyboardType = TextInputType.text,
    this.onChanged,
    this.onSaved,
    this.onTap,
    this.focusNode,
    this.inputFormatters,
    this.validator,
    this.showRequiredLabel = false,
    this.autofocus = false,
    this.textCapitalization = TextCapitalization.none,
    this.initialValue,
    this.autoCorrect = false,
    this.prefix,
    this.labelStyle,
    this.textController,
  }) : super(key: key);

  @override
  OmegaTextFieldState createState() => OmegaTextFieldState();
}

class OmegaTextFieldState extends State<OmegaTextField> {
  final _controller = TextEditingController();

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) => _initialize());
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _initialize() async {
    if (widget.initialValue != null) {
      this.setSelected(widget.initialValue!);
    }
  }

  /**
   * Set the visibility of the field
   */
  void setVisibility(bool visible) {
    setState(() {
      widget.visible = visible;
    });
  }

  void setSelected(String selected) {
    setState(() {
      widget.textController?.text = selected;
      _controller.text = selected;
    });
  }

  void clearSelection() {
    setState(() {
      widget.textController?.text = '';
      _controller.text = '';
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.visible
        ? Container(
            padding: EdgeInsets.only(top: 4, bottom: 4),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 7, bottom: 5, top: 5),
                      child: Text(
                        widget.showRequiredLabel
                            ? widget.label + ' *'
                            : widget.label,
                        textAlign: TextAlign.left,
                        style: widget.labelStyle != null
                            ? widget.labelStyle
                            : TextStyle(
                                color: widget.labelColor,
                                fontSize: widget.labelSize),
                      ),
                    )
                  ],
                ),
                TextFormField(
                  enabled: widget.enabled,
                  style: TextStyle(
                    color: widget.enabled ? Colors.black : Colors.grey,
                    fontSize: widget.labelSize,
                  ),
                  autocorrect: widget.autoCorrect,
                  maxLength: widget.maxLength,
                  textCapitalization: widget.textCapitalization,
                  autofocus: widget.autofocus,
                  controller: widget.textController == null
                      ? _controller
                      : widget.textController,
                  inputFormatters: widget.inputFormatters,
                  focusNode: widget.focusNode,
                  onChanged: widget.onChanged,
                  onSaved: widget.onSaved,
                  onTap: widget.onTap,
                  keyboardType: widget.keyboardType,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintText: widget.hint,
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    prefixIcon: widget.prefix,
                    suffixIcon: widget.showPassButton
                        ? IconButton(
                            color: Colors.grey,
                            icon: Icon(widget.obscureText
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onPressed: () {
                              setState(() {
                                widget.obscureText =
                                    widget.obscureText ? false : true;
                              });
                            },
                          )
                        : SizedBox(
                            width: 1,
                            height: 1,
                          ),
                    contentPadding: EdgeInsets.all(15.0),
                    border: InputBorder.none,
                    enabledBorder: OmegaLibConfig.enabledBorder,
                    focusedBorder: OmegaLibConfig.focusedBorder,
                    focusedErrorBorder: OmegaLibConfig.focusedErrorBorder,
                    errorBorder: OmegaLibConfig.errorBorder,
                    errorStyle: OmegaLibConfig.errorStyle,
                    disabledBorder: OmegaLibConfig.disabledBorder,
                  ),
                  obscureText: widget.obscureText,
                  validator: widget.validator,
                )
              ],
            ),
          )
        : Container();
  }
}

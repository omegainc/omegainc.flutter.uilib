import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:omegainc_lib/omegalibconfig.dart';
import 'package:omegalib_example/view/start.view.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  @override
  void initState() {
    // TODO: implement initState

    OmegaLibConfig.enabledBorder = OutlineInputBorder(
      borderSide: BorderSide(color: Colors.amber, width: 0.0),
      borderRadius: const BorderRadius.all(const Radius.circular(10.5)),
    );

    OmegaLibConfig.focusedBorder = OutlineInputBorder(
      borderSide: BorderSide(color: Colors.greenAccent, width: 0.0),
      borderRadius: const BorderRadius.all(const Radius.circular(10.5)),
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      title: 'Omega Lib Example',
      initialRoute: '/',
      routes: {
        '/': (context) => StartView(),
      },
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        Locale("pt"),
        Locale("pt_BR"),
      ],
      theme: ThemeData(
        primarySwatch: Colors.blue,
        useMaterial3: false
      ),
      color: Colors.black,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateTimeUtil {
  static DateTime removeDaysToDateTime(DateTime date, int days) {
    var data = new DateTime(date.year, date.month, date.day - days, date.hour,
        date.minute, date.second);
    return data;
  }

  static DateTime addDaysToDateTime(DateTime date, int days) {
    var data = new DateTime(date.year, date.month, date.day + days, date.hour,
        date.minute, date.second);
    return data;
  }

  static int diffDays(DateTime minorDate, DateTime majorDate) {
    return majorDate.difference(minorDate).inDays;
  }

  static DateTime getLastDateOfMonth() {
    var now = DateTime.now();

    var data = new DateTime(now.year, now.month + 1, 0);
    return data;
  }

  static DateTime getFirstDateOfMonth() {
    var now = DateTime.now();

    var data = new DateTime(now.year, now.month, 1);
    return data;
  }

  static DateTime parse(String value, String format) {
    return new DateFormat(format).parse(value);
  }

  static String format(DateTime value, String format) {
    return DateFormat(format).format(value);
  }

  static DateTime convertFromStringPtBr(String value) {
    return new DateFormat('dd/MM/yyyy').parse(value);
  }

  static String formatTimeOfDay(TimeOfDay tod) {
    final now = new DateTime.now();
    final dt = DateTime(now.year, now.month, now.day, tod.hour, tod.minute);
    return DateFormat("HH:mm:ss").format(dt);
  }

  static String formatDateTime(DateTime value) {
    return DateFormat("dd/MM/yyyy HH:mm:ss").format(value);
  }

  static String formatDateTimeFromString(String value) {
    var date = DateTime.parse(value);
    return DateFormat("dd/MM/yyyy HH:mm:ss").format(date);
  }

  static String formatDateTimeISO8601(DateTime value) {
    return DateFormat("yyyy-MM-ddTHH:mm:ss").format(value);
  }

  static String formatDate(DateTime value) {
    return DateFormat("dd/MM/yyyy").format(value);
  }

  static TimeOfDay convertStringToTimeOfDay(String s) {
    TimeOfDay hour = TimeOfDay(
        hour: int.parse(s.split(":")[0]), minute: int.parse(s.split(":")[1]));
    return hour;
  }
}

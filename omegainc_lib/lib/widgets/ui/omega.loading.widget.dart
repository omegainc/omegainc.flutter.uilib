import 'package:flutter/material.dart';

class OmegaLoadingWidget extends StatefulWidget {
  String message;
  Color textColor;

  OmegaLoadingWidget({this.message = 'Aguarde...', required this.textColor});

  @override
  _OmegaLoadingWidgetState createState() => _OmegaLoadingWidgetState();
}

class _OmegaLoadingWidgetState extends State<OmegaLoadingWidget> {
  bool validMessage() {
    return (widget.message != null && widget.message.isNotEmpty);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Card(
          child: Container(
            padding: EdgeInsets.all(25),
            child: Column(
              children: [
                CircularProgressIndicator(),
                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Text(
                    !this.validMessage()
                        ? 'Carregando...'
                        : this.widget.message,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 17,
                      color: widget.textColor,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}

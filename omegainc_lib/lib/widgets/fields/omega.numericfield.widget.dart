import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../omegalibconfig.dart';

// ignore: must_be_immutable
class OmegaNumericField extends StatefulWidget {
  bool visible;
  bool enabled;
  Color labelColor = Colors.black;
  bool showRequiredLabel = false;
  bool autofocus = false;
  String? label = "";
  double labelSize = 14;
  String? hint = "";
  int? initialValue;

  TextStyle? labelStyle;

  String? Function(int?)? validator;
  Function(int)? onChanged;
  Function(int?)? onSaved;
  Function()? onTap;
  Function()? onEditingComplete;
  Function(int?)? onFieldSubmitted;
  FocusNode? focusNode = new FocusNode();

  OmegaNumericField({
    Key? key,
    this.enabled = true,
    this.visible = true,
    this.label = '',
    this.labelSize = 14,
    this.labelColor = Colors.black,
    this.hint,
    this.onChanged,
    this.onSaved,
    this.onTap,
    this.onEditingComplete,
    this.onFieldSubmitted,
    this.focusNode,
    this.validator,
    this.showRequiredLabel = false,
    this.autofocus = false,
    this.initialValue,
    this.labelStyle,
  }) : super(key: key);

  @override
  OmegaNumericFieldState createState() => OmegaNumericFieldState();
}

class OmegaNumericFieldState extends State<OmegaNumericField> {
  final TextEditingController _controller = new TextEditingController();
  int? selectedValue;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) => _initialize());
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _initialize() async {
    if (widget.initialValue != null) {
      this.setSelected(widget.initialValue!);
    }
  }

  /**
   * Set the visibility of the field
   */
  void setVisibility(bool visible) {
    setState(() {
      widget.visible = visible;
    });
  }

  void setSelected(int selected) {
    setState(() {
      selectedValue = selected;
      _controller.text = selected.toString();
    });
  }

  void clearSelection() {
    setState(() {
      selectedValue = null;
      _controller.text = '';
    });
  }

  int _convertValue(String? value) {
    if (value == null) return 0;

    String _onlyDigits = value.replaceAll(RegExp('[^0-9]'), "");
    _onlyDigits = _onlyDigits.isEmpty ? "0" : _onlyDigits;

    int _intValue = int.tryParse(_onlyDigits) ?? 0;

    return _intValue;
  }

  @override
  Widget build(BuildContext context) {
    return widget.visible
        ? Container(
            padding: EdgeInsets.only(top: 4, bottom: 4),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 7, bottom: 5, top: 5),
                      child: Text(
                        widget.showRequiredLabel
                            ? widget.label! + ' *'
                            : widget.label!,
                        textAlign: TextAlign.left,
                        style: widget.labelStyle != null
                            ? widget.labelStyle
                            : TextStyle(
                                color: widget.labelColor,
                                fontSize: widget.labelSize,
                              ),
                      ),
                    )
                  ],
                ),
                TextFormField(
                  style: TextStyle(
                    color: widget.enabled ? Colors.black : Colors.grey,
                    fontSize: widget.labelSize,
                  ),
                  enabled: widget.enabled,
                  textCapitalization: TextCapitalization.none,
                  autofocus: widget.autofocus,
                  controller: _controller,
                  focusNode: widget.focusNode,
                  onEditingComplete: widget.onEditingComplete,
                  onFieldSubmitted: (value) {
                    if (widget.onFieldSubmitted != null) {
                      widget.onFieldSubmitted!(value.isEmpty ? 0 : int.tryParse(value));
                    }
                  },
                  onChanged: (value) {
                    setState(() {
                      selectedValue = int.tryParse(value);
                    });

                    if (widget.onChanged != null) {
                      widget.onChanged!(int.tryParse(value) ?? 0);
                    }
                  },
                  onSaved: (value) {
                    if (widget.onSaved != null) {
                      widget.onSaved!(_convertValue(value));
                    }
                  },
                  onTap: widget.onTap,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintText: widget.hint,
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    contentPadding: EdgeInsets.all(15.0),
                    border: InputBorder.none,
                    enabledBorder: OmegaLibConfig.enabledBorder,
                    focusedBorder: OmegaLibConfig.focusedBorder,
                    focusedErrorBorder: OmegaLibConfig.focusedErrorBorder,
                    errorBorder: OmegaLibConfig.errorBorder,
                    errorStyle: OmegaLibConfig.errorStyle,
                  ),
                  validator: (value) {
                    if (widget.validator == null) {
                      return null;
                    } else {
                      return widget.validator!(_convertValue(value));
                    }
                  },
                )
              ],
            ),
          )
        : Container();
  }
}

// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_typeahead/flutter_typeahead.dart';

// import '../../omegalibconfig.dart';

// // ignore: must_be_immutable
// class OmegaAutoCompleteField extends StatefulWidget {
//   List<String> suggestions = <String>[];
//   Color? labelColor = Colors.black;
//   double labelSize;
//   bool visible;
//   bool enabled;
//   bool showRequiredLabel = false;
//   bool autofocus = false;
//   String? label = "";
//   String? hint = "";
//   TextInputType keyboardType;
//   TextEditingController? controller = new TextEditingController();

//   Function()? onSuggestionSelected;
//   String? Function(String?)? validator;
//   Function(String)? onChanged;
//   Function(String?)? onSaved;
//   Function()? onTap;
//   FocusNode? focusNode = new FocusNode();

//   List<TextInputFormatter>? inputFormatters = <TextInputFormatter>[];

//   OmegaAutoCompleteField({
//     this.enabled = true,
//     this.visible = true,
//     this.labelSize = 14.0,
//     this.onSuggestionSelected,
//     required this.suggestions,
//     this.label,
//     this.labelColor,
//     this.hint,
//     this.keyboardType = TextInputType.text,
//     this.onChanged,
//     this.onSaved,
//     this.onTap,
//     this.focusNode,
//     this.inputFormatters,
//     this.validator,
//     this.showRequiredLabel = false,
//     this.controller,
//     this.autofocus = false,
//   });

//   @override
//   _OmegaAutoCompleteFieldState createState() => _OmegaAutoCompleteFieldState();

//   List<String> getSuggestions(String query) {
//     List<String> matches = [];
//     matches.addAll(suggestions);

//     matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
//     return matches;
//   }
// }

// class _OmegaAutoCompleteFieldState extends State<OmegaAutoCompleteField> {
// /**
//    * Set the visibility of the field
//    */
//   void setVisibility(bool visible) {
//     setState(() {
//       widget.visible = visible;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     bool showRequiredLabel = widget.showRequiredLabel;
//     String label = widget.label ?? '';
//     label = showRequiredLabel ? label + ' *' : label;

//     return widget.visible
//         ? Container(
//             margin: EdgeInsets.only(top: 5, bottom: 5),
//             child: Column(
//               children: <Widget>[
//                 Row(
//                   children: <Widget>[
//                     Padding(
//                       padding: EdgeInsets.only(left: 7, bottom: 5, top: 5),
//                       child: Text(
//                         label,
//                         textAlign: TextAlign.left,
//                         style: TextStyle(
//                           color: widget.labelColor,
//                           fontSize: widget.labelSize,
//                         ),
//                       ),
//                     )
//                   ],
//                 ),
//                 TypeAheadFormField(
//                   textFieldConfiguration: TextFieldConfiguration(
//                     style: TextStyle(
//                       color: widget.enabled ? Colors.black : Colors.grey,
//                       fontSize: widget.labelSize,
//                     ),
//                     enabled: widget.enabled,
//                     controller: widget.controller,
//                     keyboardType: widget.keyboardType,
//                     focusNode: widget.focusNode,
//                     autofocus: widget.autofocus,
//                     inputFormatters: widget.inputFormatters,
//                     onChanged: widget.onChanged,
//                     onTap: widget.onTap,
//                     decoration: InputDecoration(
//                       filled: true,
//                       fillColor: Colors.grey[200],
//                       hintText: widget.hint,
//                       hintStyle: TextStyle(color: Colors.grey),
//                       contentPadding: EdgeInsets.all(15.0),
//                       border: InputBorder.none,
//                       enabledBorder: OmegaLibConfig.enabledBorder,
//                       focusedBorder: OmegaLibConfig.focusedBorder,
//                       focusedErrorBorder: OmegaLibConfig.focusedErrorBorder,
//                       errorBorder: OmegaLibConfig.errorBorder,
//                       errorStyle: OmegaLibConfig.errorStyle,
//                     ),
//                   ),
//                   suggestionsCallback: (pattern) {
//                     return widget.getSuggestions(pattern);
//                   },
//                   itemBuilder: (context, suggestion) {
//                     return ListTile(
//                       title: Text(suggestion as String),
//                     );
//                   },
//                   transitionBuilder: (context, suggestionsBox, controller) {
//                     Widget result;

//                     int lenght = widget.controller?.text.length ?? 0;
//                     (lenght > 1)
//                         ? result = suggestionsBox
//                         : result = SizedBox();

//                     return result;
//                   },
//                   onSuggestionSelected: (suggestion) {
//                     setState(() {
//                       widget.controller?.text = suggestion as String;
//                     });
//                   },
//                   hideOnEmpty: true,
//                   hideOnError: true,
//                   getImmediateSuggestions: false,
//                   validator: widget.validator,
//                   onSaved: widget.onSaved,
//                 ),
//               ],
//             ),
//           )
//         : Container();
//   }
// }

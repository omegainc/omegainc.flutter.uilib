import 'package:omegainc_lib/formatters/currency.inputformatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import '../../omegalibconfig.dart';

// ignore: must_be_immutable
class OmegaMoneyField extends StatefulWidget {
  bool visible;
  bool enabled;
  Color labelColor = Colors.black;
  Color? iconColor;
  bool showRequiredLabel = false;
  bool autofocus = false;
  String label = "";
  double labelSize = 14;
  String? hint = "";
  TextInputType keyboardType = TextInputType.number;
  TextCapitalization textCapitalization = TextCapitalization.none;

  TextStyle? labelStyle;

  String? Function(double?)? validator;
  Function(double)? onChanged;
  Function(double?)? onSaved;
  Function()? onTap;
  FocusNode? focusNode = new FocusNode();

  double? initialValue;

  OmegaMoneyField(
      {Key? key,
      this.enabled = true,
      this.visible = true,
      this.label = '',
      this.labelSize = 14,
      this.labelColor = Colors.black,
      this.iconColor,
      this.hint,
      this.onChanged,
      this.onSaved,
      this.onTap,
      this.focusNode,
      this.validator,
      this.showRequiredLabel = false,
      this.autofocus = false,
      this.initialValue,
      this.labelStyle})
      : super(key: key);

  @override
  OmegaMoneyFieldState createState() => OmegaMoneyFieldState();
}

class OmegaMoneyFieldState extends State<OmegaMoneyField> {
  final TextEditingController _controller = new TextEditingController();
  double? selectedValue = 0;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) => _initialize());
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _initialize() async {
    if (widget.initialValue != null) {
      this.setSelected(widget.initialValue!);
    }
  }

  /**
   * Set the visibility of the field
   */
  void setVisibility(bool visible) {
    setState(() {
      widget.visible = visible;
    });
  }

  String formatMoney(double value) {
    final formatter = new NumberFormat("#,##0.00", "pt_BR");
    String newText = "R\$ " + formatter.format(value);
    return newText;
  }

  void setSelected(double selected) {
    setState(() {
      selectedValue = selected;

      _controller.text = formatMoney(selected);
    });
  }

  void clearSelection() {
    setState(() {
      selectedValue = null;
      _controller.text = '';
    });
  }

  double _convertValue(String? value) {
    if (value == null || value.isEmpty) value = "0";

    String _onlyDigits = value.replaceAll(RegExp('[^0-9]'), "");

    double _doubleValue = double.parse(_onlyDigits) / 100;

    return _doubleValue;
  }

  @override
  Widget build(BuildContext context) {
    return widget.visible
        ? Container(
            padding: EdgeInsets.only(top: 4, bottom: 4),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 7, bottom: 5, top: 5),
                      child: Text(
                        widget.showRequiredLabel
                            ? widget.label + ' *'
                            : widget.label,
                        textAlign: TextAlign.left,
                        style: widget.labelStyle != null
                            ? widget.labelStyle
                            : TextStyle(
                                color: widget.labelColor,
                                fontSize: widget.labelSize),
                      ),
                    )
                  ],
                ),
                TextFormField(
                  style: TextStyle(
                    color: widget.enabled ? Colors.black : Colors.grey,
                    fontSize: widget.labelSize,
                  ),
                  enabled: widget.enabled,
                  textCapitalization: widget.textCapitalization,
                  autofocus: widget.autofocus,
                  controller: _controller,
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                    CurrencyInputFormatter(maxDigits: 12),
                  ],
                  focusNode: widget.focusNode,
                  onChanged: (value) {
                    if (widget.onChanged != null) {
                      widget.onChanged!(_convertValue(value));
                    }
                  },
                  onSaved: (value) {
                    if (widget.onSaved != null) {
                      widget.onSaved!(_convertValue(value));
                    }
                  },
                  onTap: widget.onTap,
                  keyboardType: widget.keyboardType,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey[200],
                    hintText: widget.hint,
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    suffixIcon: IconButton(
                      icon: Icon(Icons.attach_money),
                      color: widget.iconColor,
                      onPressed: () {},
                    ),
                    contentPadding: EdgeInsets.all(15.0),
                    border: InputBorder.none,
                    disabledBorder: OmegaLibConfig.disabledBorder,
                    enabledBorder: OmegaLibConfig.enabledBorder,
                    focusedBorder: OmegaLibConfig.focusedBorder,
                    focusedErrorBorder: OmegaLibConfig.focusedErrorBorder,
                    errorBorder: OmegaLibConfig.errorBorder,
                    errorStyle: OmegaLibConfig.errorStyle,
                  ),
                  validator: (value) {
                    if (widget.validator == null) {
                      return null;
                    } else {
                      return widget.validator!(_convertValue(value));
                    }
                  },
                )
              ],
            ),
          )
        : Container();
  }
}

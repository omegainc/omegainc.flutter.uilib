import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class OmegaEmptyWidget extends StatefulWidget {
  String message;
  Color textColor;
  double? bottomMargin;

  OmegaEmptyWidget({
    this.message = '',
    this.textColor = Colors.grey,
    this.bottomMargin,
  });

  @override
  _OmegaEmptyWidgetState createState() => _OmegaEmptyWidgetState();
}

class _OmegaEmptyWidgetState extends State<OmegaEmptyWidget> {
  @override
  Widget build(BuildContext context) {
    if (widget.bottomMargin == null) {
      widget.bottomMargin = MediaQuery.of(context).size.height * 0.15;
    }

    return Center(
      child: Container(
        margin: EdgeInsets.only(
          left: 10,
          right: 10,
          bottom: widget.bottomMargin!,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Lottie.asset(
              'assets/json/empty-and-lost.json',
              package: 'omegainc_lib',
              height: MediaQuery.of(context).size.height * 0.35,
              repeat: true,
              reverse: true,
              animate: true,
            ),
            Text(
              widget.message.isNotEmpty
                  ? widget.message
                  : 'Nenhum registro encontrado.',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 17, color: widget.textColor),
            ),
          ],
        ),
      ),
    );
  }
}

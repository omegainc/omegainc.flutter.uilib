import 'package:flutter/material.dart';

// ignore: must_be_immutable
class OmegaSearchBar extends StatelessWidget {
  FocusNode? focusNode;
  Function(String)? onChanged;

  Color textcolor;

  OmegaSearchBar({this.onChanged, this.focusNode, required this.textcolor});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      focusNode: focusNode,
      style: TextStyle(color: textcolor),
      keyboardType: TextInputType.text,
      onChanged: onChanged,
      decoration: InputDecoration(
        hintText: 'Pesquisar',
        hintStyle: TextStyle(color: textcolor),
        contentPadding: EdgeInsets.all(15.0),
        border: InputBorder.none,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.transparent, width: 0.0),
          borderRadius: const BorderRadius.all(const Radius.circular(17.5)),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.transparent, width: 0.0),
          borderRadius: const BorderRadius.all(const Radius.circular(17.5)),
        ),
      ),
    );
  }
}
